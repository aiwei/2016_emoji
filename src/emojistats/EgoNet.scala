package emojistats

import java.io.{File, PrintWriter}

import scala.collection.immutable.Map
import scala.collection.parallel.ParSet
import scala.io.Source

/**
  * Created by aiwei on 9/26/16.
  */
object EgoNet {
  def parseArgs(map: Map[String, String], list: List[String]): Map[String, String] = {
    list match {
      case Nil => map

      case "-nn-file" :: value :: tail =>
        parseArgs(map ++ Map(("nnFile", value)), tail)
      case "-out-file" :: value :: tail =>
        parseArgs(map ++ Map(("outFile", value)), tail)
      case "-k" :: value :: tail =>
        parseArgs(map ++ Map(("k", value)), tail)

      case opt :: tail =>
        println("unknown option:" + opt)
        parseArgs(map, tail)
    }
  }

  def main(args: Array[String]): Unit = {
    val argMap = parseArgs(Map[String, String](), args.toList)
    val inFile = argMap.getOrElse("nnFile", "")
    val outFile = argMap.getOrElse("outFile", "")
    val k = argMap.getOrElse("k", "20").toInt

    val poi = Source.fromFile(inFile).getLines.map { line =>
      val tup = line.split("\t")
      (tup(0).toInt, tup(1).toInt, tup(2).toFloat)
    }.filter(_._1 < 1281).map(_._2).toSet

    val knn = Source.fromFile(inFile).getLines.map { line =>
      val tup = line.split("\t")
      (tup(0).toInt, (tup(1).toInt, tup(2).toFloat))
    }.filter{tup =>
      (tup._1 < 1281 || poi(tup._1)) && tup._2._2 > 0
    }.toArray.groupBy(_._1).toList.par.flatMap{
      case (word:Int, neighbors:Array[(Int, (Int, Float))]) =>
        neighbors.sortBy{_._2._2}.take(k).flatMap{case (word1, (neighbor, dist)) =>
          List((word1, neighbor), (neighbor, word1))
        }
    }.toSet

    val writer = new PrintWriter(new File(outFile))
    writer.write("emoij_id\tdegree\tlcc\n")

    knn.filter{_._1 < 1281}.groupBy(_._1).par.map{
      case (emoji: Int, neighbors: ParSet[(Int, Int)]) =>
        val degree = neighbors.size
        val connected = neighbors.flatMap { case (emoji_inner1:Int, neighbor1:Int) =>
          neighbors.filter{ case (emoji_inner2:Int, neighbor2:Int) =>
            (neighbor1 < neighbor2) && knn((neighbor1,neighbor2))
          }
        }.size
        (emoji, degree, connected * 2.0 / degree / (degree - 1))
    }.foreach{
      case (emoji, degree, lcc) =>
        writer.write("%d\t%d\t%f\n".format(emoji, degree, lcc))
    }

    writer.close()
  }
}
