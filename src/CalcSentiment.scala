import breeze.linalg.DenseVector
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.immutable.Map
import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
  * Created by aiwei on 10/16/16.
  */
object CalcSentiment {
  def parseArgs(map: Map[String, String], list: List[String]): Map[String, String] = {
    list match {
      case Nil => map

      case "-lex-file" :: value :: tail =>
        parseArgs(map ++ Map(("lexFile", value)), tail)
      case "-msg-file" :: value :: tail =>
        parseArgs(map ++ Map(("msgFile", value)), tail)
      case "-out-file" :: value :: tail =>
        parseArgs(map ++ Map(("outFile", value)), tail)

      case opt :: tail =>
        println("unknown option:" + opt)
        parseArgs(map, tail)
    }
  }

  def main(args: Array[String]): Unit = {
    val argMap = parseArgs(Map[String, String](), args.toList)

    val lexFile = argMap.getOrElse("lexFile", "s3://beidaemr/aiwei/lexicon/liwc/*")
    val msgFile = argMap.getOrElse("msgFile", "s3://beidaemr/aiwei/sentences/*")
    val outFile = argMap.getOrElse("outFile", "s3://beidaemr/aiwei/sentiment/liwc")

    val sc = new SparkContext(new SparkConf())

    // load lexicon
    val lexMap = sc.broadcast(sc.textFile(lexFile, minPartitions = 200).distinct().map{line =>
      val tup = line.split("\t")
      (tup(0), (tup(1).toDouble, tup(2).toDouble, tup(3).toInt))
    }.collect().toMap)

    // load emoji
    val emojiMap = sc.broadcast(EmojiConst.emojiIndex.filter(tup => lexMap.value.contains(tup._1)).toMap)

    sc.textFile(msgFile).distinct().map { line =>
      val words = line.split(" ")
      var validLen = 0
      var emojiLen = 0
      val emojiCount = mutable.HashSet[String]()
      var validPos = 0.0
      var validNeg = 0.0
      var emojiPos = 0.0
      var emojiNeg = 0.0

      words.foreach{ word =>
        if (lexMap.value.contains(word)) {
          val tup = lexMap.value(word)
            if (emojiMap.value.contains(word)) {
              emojiCount += word
              emojiPos += tup._1
              emojiNeg += tup._2
              emojiLen += 1
            } else {
              validPos += tup._1
              validNeg += tup._2
              validLen += 1
            }
        }
      }
      // (length, valid len, emoji len, uniq emoji, avg pos, avg neg, valid pos, valid neg, emoji pos, emoji neg.
      (words.length, validLen, emojiLen, emojiCount.size,
        calcAvg(validPos + emojiPos, validLen + emojiLen),
        calcAvg(validNeg + emojiNeg, validLen + emojiLen),
        calcAvg(validPos, validLen),
        calcAvg(validNeg, validLen),
        calcAvg(emojiPos, emojiLen),
        calcAvg(emojiNeg, emojiLen))
    }.map{ tup =>
      "%d\t%d\t%d\t%d\t%f\t%f\t%f\t%f\t%f\t%f".format(tup._1,tup._2, tup._3, tup._4,
        tup._5, tup._6, tup._7, tup._8, tup._9, tup._10)
    }.saveAsTextFile(outFile)
  }
  def calcAvg(s: Double, l:Int):Double={
    l match {
      case 0 => 0.0
      case _ => s / l
    }
  }
}
