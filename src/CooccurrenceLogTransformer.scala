import java.io.{File, PrintWriter}

import scala.collection.immutable.Map
import scala.io.Source

/**
  * Created by aiwei on 9/20/16.
  */
object CooccurrenceLogTransformer {

  def parseArgs(map : Map[String, String], list: List[String]) : Map[String, String] = {
    list match {
      case Nil => map

      case "-in-file" :: value :: tail =>
        parseArgs(map ++ Map(("inFile", value)), tail)
      case "-out-file" :: value :: tail =>
        parseArgs(map ++ Map(("outFile", value)), tail)

      case opt :: tail =>
        println("unknown option:" + opt)
        parseArgs(map, tail)
    }
  }
  def main(args: Array[String]): Unit = {
    val argMap = parseArgs(Map[String, String](), args.toList)
    val writer = new PrintWriter(new File(argMap.getOrElse("outFile", "")))
    val log2 = scala.math.log(2)
    Source.fromFile(argMap.getOrElse("inFile", "")).getLines.foreach{ line =>
      val tup = line.split("\t")
      val logWeight = scala.math.log(tup(2).toInt) / log2
      if (logWeight > 0) {
        val outString = tup(0) + "\t" + tup(1) + "\t" + logWeight / log2
        writer.write(outString + "\n")
      }
    }
    writer.close()
  }
}
