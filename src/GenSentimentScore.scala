import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.immutable.Map

/**
  * Created by aiwei on 10/16/16.
  */
object GenSentimentScore {
  def parseArgs(map: Map[String, String], list: List[String]): Map[String, String] = {
    list match {
      case Nil => map

      case "-index-file" :: value :: tail =>
        parseArgs(map ++ Map(("indexFile", value)), tail)
      case "-liwc-file" :: value :: tail =>
        parseArgs(map ++ Map(("liwcFile", value)), tail)
      case "-out-file" :: value :: tail =>
        parseArgs(map ++ Map(("outFile", value)), tail)
      case "-knn-file" :: value :: tail =>
        parseArgs(map ++ Map(("knnFile", value)), tail)
      case "-k" :: value :: tail =>
        parseArgs(map ++ Map(("kNearest", value)), tail)

      case opt :: tail =>
        println("unknown option:" + opt)
        parseArgs(map, tail)
    }
  }

  def main(args: Array[String]): Unit = {
    val argMap = parseArgs(Map[String, String](), args.toList)

    val indexFile = argMap.getOrElse("indexFile", "s3://beidaemr/aiwei/index/msg/*")
    val liwcFile = argMap.getOrElse("liwcFile", "s3://beidaemr/aiwei/resource/liwc/*")
    val outFile = argMap.getOrElse("outFile", "s3://beidaemr/aiwei/lexicon/liwc")
    val knnFile = argMap.getOrElse("knnFile", "s3://beidaemr/aiwei/k_nearest/word/win_log_line2/*")
    val k = argMap.getOrElse("kNearest", "20").toInt
    val threshold = argMap.getOrElse("threshold", "5").toInt

    val sc = new SparkContext(new SparkConf())

    // load existing score
    val score_from_liwc = sc.textFile(liwcFile).distinct().filter { line =>
      val tup = line.split("\t")
      tup.length == 94
    }.filter { line =>
      val tup = line.split("\t")
      tup.head match {
        case "Source (A)" => false
        case _ => tup(1).toInt > 0
      }
    }.map { line =>
      val tup = line.split("\t").toList
      (tup.head, (tup(31).toDouble, tup(32).toDouble))
    }
    val scoreFound = sc.broadcast(score_from_liwc.collect().toMap)

    // load index (index => word)
    val indexMap = sc.broadcast(sc.textFile(indexFile).distinct().map { line =>
      val tup = line.split("\t").toList
      (tup(1).toInt, tup.head)
    }.collect().toMap)

    // go over knn, find those without score, and see if we can get it an estimated score
    val score_from_estimate = sc.textFile(knnFile).distinct().map{line =>
      val tup = line.split("\t")
      (tup(0).toInt, (tup(1).toInt, tup(2).toDouble))
    }.filter{tup =>
      !scoreFound.value.contains(indexMap.value(tup._1))
    }.groupByKey.map {
      case (wordIdx: Int, neighbors: Iterable[(Int, Double)]) =>
        neighbors.filter { tup =>
          scoreFound.value.contains(indexMap.value(tup._1)) & tup._2 > 0
        }.toArray.sortBy(_._2).take(k) match {
          case validNeighbors if validNeighbors.length > threshold =>
            val len = validNeighbors.length
            validNeighbors.map { validNeighbor =>
              scoreFound.value(indexMap.value(validNeighbor._1))
            }.reduceLeft((x, y) => (x._1 + y._1, x._2 + y._2)) match {
              case (posemo, negemo) => (indexMap.value(wordIdx), (posemo / len, negemo / len))
            }
          case _ =>
            (indexMap.value(wordIdx), (-1.0, -1.0))
        }
    }.filter(_._2._1 >= 0)

    // concate score_from_liwc and score_from_estimate, and output
    score_from_liwc.map{ tup =>
      (tup._1, (tup._2._1, tup._2._2, 0))
    }.map { tup =>
      tup._1 + "\t" + tup._2._1 + "\t" + tup._2._2 + "\t" + tup._2._3
    }.union {
      score_from_estimate.map { tup =>
        (tup._1, (tup._2._1, tup._2._2, 1))
      }.map { tup =>
        tup._1 + "\t" + tup._2._1 + "\t" + tup._2._2 + "\t" + tup._2._3
      }
    }.saveAsTextFile(outFile)

    sc.stop()
  }

}
