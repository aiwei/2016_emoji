import java.io.{File, PrintWriter}

import scala.io.Source
import java.util.regex._

/**
  * Created by aiwei on 9/20/16.
  */
object GenFilteredIndex {
  def main(args: Array[String]): Unit = {
    val outFile = "svr/filtered_embedding/index_typed"
    val inFile = "svr/index/msg"
    val writer = new PrintWriter(new File(outFile))

    val emoticonPattern = Pattern.compile("^" + Twokenize.OR(Twokenize.emoticon,
      Twokenize.Hearts, Twokenize.Arrows, Twokenize.punctChars, Twokenize.punctSeq) + "$")

    Source.fromFile(inFile).getLines().foreach{line =>
      val tup = line.split("\t")
      val word = tup(0)
      val index = tup(1).toInt
      val count = tup(2).toInt
      if (count >= 2000) {
        var tokenType = "other"
        if (index < 1281) {
          tokenType = "emoji"
        } else {
          val matches = emoticonPattern.matcher(word)
          if (matches.find()) {
            tokenType = "emoticon"
          } else {
            if (word matches """^[a-zA-Z0-9'%&\$@#.,\-_\/:]+$""") {
              tokenType  = "word"
            }
          }
        }
        writer.write("%d\t%s\t%s\n".format(index, word, tokenType))
      }
    }
    writer.close()
  }
}
