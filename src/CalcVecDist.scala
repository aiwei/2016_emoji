import breeze.linalg.{DenseVector, norm}
import org.apache.spark.{HashPartitioner, SparkConf, SparkContext}

import scala.collection.immutable.Map
import scala.collection.mutable
import scala.collection.mutable.ListBuffer

import org.apache.hadoop.mapred.lib.MultipleTextOutputFormat

/**
  * Created by aiwei on 10/3/16.
  */
object CalcVecDist {
  def parseArgs(map: Map[String, String], list: List[String]): Map[String, String] = {
    list match {
      case Nil => map
      case "-embedding-file" :: value :: tail =>
        parseArgs(map ++ Map(("embeddingFile", value)), tail)
      case "-index-file" :: value :: tail =>
        parseArgs(map ++ Map(("indexFile", value)), tail)
      case "-sentence-file" :: value :: tail =>
        parseArgs(map ++ Map(("sentenceFile", value)), tail)
      case "-out-file" :: value :: tail =>
        parseArgs(map ++ Map(("outFile", value)), tail)
      case "-d" :: value :: tail =>
        parseArgs(map ++ Map(("d", value)), tail)

      case "-gen-cooccur" :: tail =>
        parseArgs(map ++ Map(("genCooccur", "1")), tail)
      case "-gen-index" :: tail =>
        parseArgs(map ++ Map(("genIndex", "1")), tail)

      case opt :: tail =>
        println("unknown option:" + opt)
        parseArgs(map, tail)
    }
  }

  def main(args: Array[String]): Unit = {
    val argMap = parseArgs(Map[String, String](), args.toList)

    println(argMap)

    val sc = new SparkContext(new SparkConf())

    val embeddingFile = argMap.getOrElse("embeddingFile", "s3://beidaemr/aiwei/embedding/win_log_line2")
    val indexFile = argMap.getOrElse("indexFile", "s3://beidaemr/aiwei/index/win")
    val sentenceFile = argMap.getOrElse("sentenceFile", "s3://beidaemr/aiwei/sentences/*")
    val outFile = argMap.getOrElse("outFile", "s3://beidaemr/aiwei/vec_dist/win_log_line2")
    val d = argMap.getOrElse("d", "100").toInt


    // load index
    val indexMap = sc.textFile(indexFile).distinct().map{ line =>
      val tup = line.split("\t")
      print(",%d,".format(tup.length))
      //(index, word)
      (tup(1), tup(0))
    }.collect().toMap

    val indexMapBC = sc.broadcast(indexMap)

    // load embedding
    val embedding = sc.textFile(embeddingFile, minPartitions = 200).distinct().map{ line =>
      val lst = line.split(" ")
      (indexMapBC.value.getOrElse(lst.head, ""), DenseVector(lst.drop(1).map(_.toDouble)))
    }.persist(org.apache.spark.storage.StorageLevel.MEMORY_AND_DISK)

    val emojiMap = sc.broadcast(EmojiConst.emojiIndex.toMap)
    val emojiEmbedding = sc.broadcast(
      embedding.filter{ case (word, vec) => emojiMap.value.contains(word)}.collect().toMap)
    val zero = sc.broadcast(DenseVector.zeros[Double](d))

    // iterate over sentences
    sc.textFile(sentenceFile).distinct().zipWithUniqueId().flatMap {
      case (line: String, msgID: Long) =>
        line.split(" ").map{ word =>
          (word, msgID)
        }
    }.join(embedding).map { case (word: String, (msgID: Long, vec: DenseVector[Double])) =>
      (msgID, (word, vec))
    }.groupByKey.flatMap { case (msgID: Long, wordvecs: Iterable[(String, DenseVector[Double])]) =>
      val validVec = DenseVector.zeros[Double](d)
      var validLen = 0
      val emojiCount = mutable.Map[String, Int]().withDefaultValue(0)

      wordvecs.foreach { case (word: String, vec:DenseVector[Double]) =>
        emojiMap.value.get(word) match {
          case Some(emoji) =>
            emojiCount(word) += 1
          case _ =>
            validVec += vec
            validLen += 1
        }
      }
      // (emoji_id, validLength, emojiLength, uniqEmoji, emojiCount, origNorm, dist
      val buffer = new ListBuffer[(Int, Int, Int, Int, Int, Double, Double)]
      if (emojiCount.nonEmpty) {
        val uniqEmoji = emojiCount.size
        val emojiLength = emojiCount.valuesIterator.sum

        val avgVec = validLen match {
          case 0 => zero.value
          case _ => validVec / validLen.toDouble
        }

        emojiCount.foreach{ case (emoji:String, cnt:Int) =>
          val emojiVec = emojiEmbedding.value(emoji)
          buffer.append((emojiMap.value(emoji), validLen, emojiLength, uniqEmoji, cnt,
            norm(avgVec), norm(avgVec - emojiVec)))
        }
      }
      buffer
    }.map{
      case (emojiId, validLen, emojiLen, uniqEmoji, emojiCnt, origNorm, dist) =>
        (emojiId.toString, "%d %d %d %d %d %f %f".format(
          emojiId, validLen, emojiLen, uniqEmoji, emojiCnt, origNorm, dist))
    }.partitionBy(new HashPartitioner(2000)).saveAsHadoopFile(outFile, classOf[String], classOf[String],
      classOf[RDDMultipleTextOutputFormat])
  }
}
