import breeze.linalg.{DenseVector, norm}
import org.apache.spark.{HashPartitioner, SparkConf, SparkContext}

import scala.collection.immutable.Map
import scala.collection.mutable
import scala.collection.mutable.ListBuffer

import org.apache.hadoop.mapred.lib.MultipleTextOutputFormat

/**
  * Created by aiwei on 10/3/16.
  */
object CalcNeighborCooccur {
  def parseArgs(map: Map[String, String], list: List[String]): Map[String, String] = {
    list match {
      case Nil => map
      case "-index-file" :: value :: tail =>
        parseArgs(map ++ Map(("indexFile", value)), tail)
      case "-sentence-file" :: value :: tail =>
        parseArgs(map ++ Map(("sentenceFile", value)), tail)
      case "-out-file" :: value :: tail =>
        parseArgs(map ++ Map(("outFile", value)), tail)
      case "-nn-file" :: value :: tail =>
        parseArgs(map ++ Map(("nnFile", value)), tail)

      case "-gen-cooccur" :: tail =>
        parseArgs(map ++ Map(("genCooccur", "1")), tail)
      case "-gen-index" :: tail =>
        parseArgs(map ++ Map(("genIndex", "1")), tail)

      case opt :: tail =>
        println("unknown option:" + opt)
        parseArgs(map, tail)
    }
  }

  def main(args: Array[String]): Unit = {
    val argMap = parseArgs(Map[String, String](), args.toList)

    println(argMap)

    val sc = new SparkContext(new SparkConf())

    val indexFile = argMap.getOrElse("indexFile", "s3://beidaemr/aiwei/filtered_embedding/index_typed")
    val sentenceFile = argMap.getOrElse("sentenceFile", "s3://beidaemr/aiwei/sentences/*")
    val outFile = argMap.getOrElse("outFile", "s3://beidaemr/aiwei/nn_cooccur/win_log_line2")
    val nnFile = argMap.getOrElse("nnFile", "s3://beidaemr/aiwei/k_nearest/word/win_log_line2/*")

    // load index
    val indexMap = sc.broadcast(
      sc.textFile(indexFile).distinct().map{ line =>
        val tup = line.split("\t")
        //(index, word)
        (tup(0).toInt, tup(1))
      }.collect().toMap
    )
    //load inverse index
    val revertIndexMap = sc.broadcast(
      sc.textFile(indexFile).distinct().map{ line =>
        val tup = line.split("\t")
        //(word, index)
        (tup(1), tup(0).toInt)
      }.collect().toMap
    )

    // load emoji index map
    val emojiMap = sc.broadcast(EmojiConst.emojiIndex.toMap)

    // load nn map
    val nnMap = sc.broadcast(
      sc.textFile(nnFile).distinct().map{line =>
        val tup = line.split("\t")
        (tup(0).toInt, (tup(1).toInt, tup(2).toDouble))
      }.filter { tup =>
        tup._1 < 1281
      }.groupByKey.map {
        case (wordIdx: Int, neighbors: Iterable[(Int, Double)]) =>
          (indexMap.value(wordIdx), indexMap.value(neighbors.toArray.sortBy(_._2).head._1))
      }.collect().toMap
    )

    val nnSet = sc.broadcast(nnMap.value.values.toSet)

    // iterate over sentences
    sc.textFile(sentenceFile).distinct().flatMap { line =>
      val tup = line.split(" ").toList
      val msgLength = tup.length
      val wordCount = new mutable.HashMap[String, Int]().withDefaultValue(0)
      // (Length, word, isEmoji&neighbor_cooccur, 1)
      val buffer = new ListBuffer[((Int, Int, Int), Int)]()
      buffer.append(((msgLength, -1, 0), 1))
      tup.foreach{ word =>
        // if it is an emoji or a nearest neighbor
        if (nnSet.value.contains(word)|nnMap.value.contains(word)) {
          wordCount(word) += 1
          // if it is a word
          if(nnSet.value.contains(word)){
            buffer.append(((msgLength, revertIndexMap.value(word), 0), 1))
          }
        }
      }
      wordCount.foreach{ case (word, cnt) =>
        nnMap.value.get(word) match{
            // if it is an emoji
          case Some(neighbor) =>
            buffer.append(((msgLength - wordCount(word), revertIndexMap.value(word), 0), 1))
            // if it's neighbor is in the message
            if (wordCount(neighbor) > 0){
              buffer.append(((msgLength - wordCount(word), revertIndexMap.value(word), 1), 1))
            }
          case _ =>
        }
      }
      buffer
    }.reduceByKey(_ + _).map{ tup =>
      "%d\t%d\t%d\t%d".format(tup._1._1, tup._1._2, tup._1._3, tup._2)
    }.saveAsTextFile(outFile)
  }
}
