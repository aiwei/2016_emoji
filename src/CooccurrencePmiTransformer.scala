import java.io.{File, PrintWriter}

import scala.collection.immutable.Map
import scala.io.Source

/**
  * Created by aiwei on 2/17/16.
  */
object CooccurrencePmiTransformer {
  def parseArgs(map : Map[String, String], list: List[String]) : Map[String, String] = {
    list match {
      case Nil => map

      case "-in-file" :: value :: tail =>
        parseArgs(map ++ Map(("inFile", value)), tail)
      case "-out-file" :: value :: tail =>
        parseArgs(map ++ Map(("outFile", value)), tail)
      case "-count-file" :: value :: tail =>
        parseArgs(map ++ Map(("countFile", value)), tail)
      case "-index-file" :: value :: tail =>
        parseArgs(map ++ Map(("indexFile", value)), tail)

      case opt :: tail =>
        println("unknown option:" + opt)
        parseArgs(map, tail)
    }
  }
  def main(args: Array[String]): Unit = {
    val argMap = parseArgs(Map[String, String](), args.toList)

    val inFile = argMap.getOrElse("inFile", "")
    val outFile = argMap.getOrElse("outFile", "")
    val countFile = argMap.getOrElse("countFile", "")
    val indexFile = argMap.getOrElse("indexFile", "")

    println("hello")

    val log2 = math.log(2)

    val logCountMap: Map[String, Double] = Source.fromFile(countFile).getLines.map{ line =>
      val tup = line.split("\t")
      (tup(0), math.log(tup(1).toInt) / log2)
    }.toMap
    println("finished loading count")

    val logTotal = logCountMap.get("[EOW]") match {
      case Some(x) => x
      case None =>
        println("cannot find the total number")
        return
    }

    val indexLogCountMap: Map[String, Double] = Source.fromFile(indexFile).getLines.map {line =>
      val tup = line.split("\t")
      val logCount:Double = logCountMap.getOrElse(tup(0), 0)
      (tup(1), logCount)
    }.toMap
    println("finished loading index")

    val writer = new PrintWriter(new File(outFile))
    Source.fromFile(inFile).getLines.foreach{ line =>
      val tup = line.split("\t")
      val pa: Double = indexLogCountMap.getOrElse(tup(0), 0)
      val pb: Double = indexLogCountMap.getOrElse(tup(1), 0)
      if (pa != 0 | pb != 0) {
        val pab = math.log(tup(2).toInt) / log2
        val pmi = pab - pa - pb + logTotal
        if (pmi > 0) {
          val outString = tup(0) + "\t" + tup(1) + "\t" + pmi
          writer.write(outString + "\n")
        }
      }
    }
    writer.close()
  }
}
