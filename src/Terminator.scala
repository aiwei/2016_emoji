import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by aiwei on 9/21/16.
  */
object Terminator {
  def main(args: Array[String]): Unit = {

    val sc = new SparkContext(new SparkConf())
    sc.parallelize(seq = Array("foo", "bar")).saveAsTextFile("s3://beidaemr/aiwei/foobar")
    sc.parallelize(seq = Array("foo", "bar")).saveAsTextFile("s3://beidaemr/aiwei/foobar")
    sc.stop()
  }
}