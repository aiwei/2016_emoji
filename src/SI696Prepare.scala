import java.security.MessageDigest
import java.util.regex.Pattern

import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.immutable.Map
import scala.collection.mutable.ListBuffer

/**
  * Created by aiwei on 8/26/16.
  */
object SI696Prepare{
  def prepare(_sc: SparkContext,
                 _metaFile: String,
                 _origFile: String,
                 _msgOutFile: String,
                 _metaOutFile: String):Unit = {

    val md5digest = MessageDigest.getInstance("MD5")
    val emojiRegex = EmojiConst.emojiRegex
    val emojiPatttern = Pattern.compile(emojiRegex)

    // extract message data from one day.

    _sc.textFile(_origFile).distinct().filter { x =>
      val a = x.split("\t")
      a.length > 2 && a(2).length == 32 && a(1).length > 0
    }.map{ line =>
      val tup = line.split("\t")
      val msg = new String(tup(1).getBytes("UTF-8"), "UTF-8") // message in this record

      val emojiMatcher = emojiPatttern.matcher(msg)
      val emojiBuffer = new ListBuffer[(String, Int)]()
      while (emojiMatcher.find()) {
        val emoji = emojiMatcher.group()
        emojiBuffer.append((emoji, emojiMatcher.start())) // notice that some emojis take multiple bytes.
      }
      val emojiList = emojiBuffer.toList

      "%s\t%s\t%s\t%s\t%s".format(
        tup(2), // user_id
        tup(3), // timestamp
        tup(0), // app
        msg.length,
        emojiList.map{ pair =>
          "(%s,%d)".format(pair._1, pair._2)
        }.mkString(",")
      )

    }.coalesce(400).saveAsTextFile(_msgOutFile)

    val newRdd = _sc.textFile(_msgOutFile+"/*").map(_.split("\t")(0)).distinct().map((_, 1))

    val meta = _sc.textFile(_metaFile).distinct().filter { x =>
      val a = x.split("\t")
      a.length > 10 && a(0).length() == 32// && a(6) == "us"
    }.map { x =>
      val a = x.split("\t")
      (a(0), (a(6), a(10)))
    }.join(newRdd).map { case (uid: String, ((country: String, lang: String), _)) =>
      "%s\t%s\t%s".format(uid, country, lang)
    }.saveAsTextFile(_metaOutFile)
  }

  def parseArgs(map: Map[String, String], list: List[String]): Map[String, String] = {
    list match {
      case Nil => map

      case "-meta-file" :: value :: tail =>
        parseArgs(map ++ Map(("metaFile", value)), tail)
      case "-orig-file" :: value :: tail =>
        parseArgs(map ++ Map(("origFile", value)), tail)
      case "-mid-file" :: value :: tail =>
        parseArgs(map ++ Map(("midFile", value)), tail)

      case opt :: tail =>
        println("unknown option:" + opt)
        parseArgs(map, tail)
    }
  }

  def main(args: Array[String]): Unit = {
    println("Starting, parsing args")
    println(args)

    val argMap = parseArgs(Map[String, String](), args.toList)

    println(argMap)

    val sc = new SparkContext(new SparkConf())
    val metaFile = argMap.getOrElse("metaFile", "s3://beidas3/kikakeyboard/meta_full/20150930/")
    val origFile= argMap.getOrElse("wordFile", "s3://beidas3/kikakeyboard/word/20150901/*/")
    val msgOutFile = argMap.getOrElse("midFile", "s3://beidaemr/aiwei/si696/20150901")
    val metaOutFile = argMap.getOrElse("metaFile", "s3://beidaemr/aiwei/si696/meta")
    prepare(_sc = sc, _metaFile = metaFile, _origFile = origFile, _msgOutFile = msgOutFile, _metaOutFile = metaOutFile)

    sc.stop()
  }
}
