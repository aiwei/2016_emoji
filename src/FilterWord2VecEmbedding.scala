import java.io.{File, PrintWriter}
import java.nio.charset.CodingErrorAction

import scala.collection.immutable.Map
import scala.collection.mutable.ListBuffer
import scala.io.{Codec, Source}

/**
  * Created by aiwei on 9/20/16.
  */
object FilterWord2VecEmbedding {
  def parseArgs(map: Map[String, String], list: List[String]): Map[String, String] = {
    list match {
      case Nil => map

      case "-in-file" :: value :: tail =>
        parseArgs(map ++ Map(("inFile", value)), tail)
      case "-out-file" :: value :: tail =>
        parseArgs(map ++ Map(("outFile", value)), tail)

      case opt :: tail =>
        println("unknown option:" + opt)
        parseArgs(map, tail)
    }
  }
  def main(args: Array[String]): Unit = {
    val argMap = parseArgs(Map[String, String](), args.toList)

    val inFile = argMap.getOrElse("inFile", "")
    val outFile = argMap.getOrElse("outFile", "")

    val filteredIndexFile = "svr/filtered_embedding/index_typed"

    // filter index -- filter index/msg
    val indexBuffer = new ListBuffer[(String, Int)]
    Source.fromFile(filteredIndexFile).getLines.map{ line =>
      val tup = line.split("\t")
      (tup(0).toInt, tup(1), tup(2))
    }.foreach { case (idx: Int, word: String, typ: String) =>
      indexBuffer.append((word, idx))
    }

    val msgIndexMap = indexBuffer.toMap

    val decoder = Codec.UTF8.decoder.onMalformedInput(CodingErrorAction.IGNORE)

    val writer = new PrintWriter(new File(outFile))
    Source.fromFile(inFile)(decoder).getLines().foreach{ line: String =>
      val tup = line.split(" ")
      msgIndexMap.get(tup(0)) match{
        case Some(msgIndex) =>
          writer.write((msgIndex +: tup.drop(1)).mkString(" ") + "\n")
        case None =>
      }
    }
    writer.close()

  }
}
