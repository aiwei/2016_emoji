import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.immutable.Map

/**
  * Created by aiwei on 8/26/16.
  */
object PreProcess {
  def preProcess(_sc: SparkContext,
                _metaFile: String,
                _origFile: String,
                _midFile: String):Unit = {
    val meta = _sc.textFile(_metaFile).distinct().filter { x =>
      val a = x.split("\t")
      a.length > 6 && a(0).length() == 32 && a(6) == "us"
    }.map { x =>
      val a = x.split("\t")
      (a(0), 1)
    }

    _sc.textFile(_origFile).distinct().filter { x =>
      val a = x.split("\t")
      a.length > 2 && a(2).length == 32 && a(1).length > 0
    }.map { line =>
      val tup = line.split("\t")
      // (user_id, time, app, text)
      (tup(2), (tup(3), tup(0), tup(1)))
    }.join(meta).map {
      case (duid: String, ((time: String, app: String, text: String), 1)) =>
        duid + "\t" + time + "\t" +  app + "\t" +  text
    }.coalesce(2400).saveAsTextFile(_midFile)
  }

  def parseArgs(map: Map[String, String], list: List[String]): Map[String, String] = {
    list match {
      case Nil => map

      case "-meta-file" :: value :: tail =>
        parseArgs(map ++ Map(("metaFile", value)), tail)
      case "-orig-file" :: value :: tail =>
        parseArgs(map ++ Map(("origFile", value)), tail)
      case "-mid-file" :: value :: tail =>
        parseArgs(map ++ Map(("midFile", value)), tail)

      case opt :: tail =>
        println("unknown option:" + opt)
        parseArgs(map, tail)
    }
  }

  def main(args: Array[String]): Unit = {
    println("Starting, parsing args")
    println(args)

    val argMap = parseArgs(Map[String, String](), args.toList)

    println(argMap)

    val sc = new SparkContext(new SparkConf())
    val metaFile = argMap.getOrElse("metaFile", "s3://beidas3/kikakeyboard/meta_full/20150930/")
    val origFile= argMap.getOrElse("wordFile", "s3://beidas3/kikakeyboard/word/201509*/language=en_US/*")
    val midFile = argMap.getOrElse("midFile", "s3://beidaemr/aiwei/preprocessed")

    preProcess(_sc = sc, _metaFile = metaFile, _origFile = origFile, _midFile = midFile)

    sc.stop()
  }
}
