import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.immutable.Map
import scala.collection.mutable.ListBuffer
import scala.math._
import collection.JavaConverters._

/**
  * Created by aiwei on 9/20/16.
  */
object GenCooccur {

  def genCooccur(_sc: SparkContext,
                 _dataFile: String,
                 _cooccurFile: String,
                 _window: Int): Unit = {

    _sc.textFile(_dataFile).distinct().filter { _.split("\t").length > 3
    }.map { _.split("\t")(3)
    }.flatMap { content =>
      val buffer = new ListBuffer[((String, String), Int)]
      val tokens = Twokenize.tokenize(content).asScala.toArray
      val length = tokens.length
      val window = if (_window == 0) length else min(_window, length)

      for (i <- 0 to length - window) {
        var wordSet = Set[String]()
        for (j <- i until i + window) {
          val tk = tokens(j)
          if (!wordSet(tk)) {
            for (k <- wordSet) {
              buffer.append(((tk, k), 1))
              buffer.append(((k, tk), 1))
            }
            wordSet += tk
            buffer.append(((tk, tk), 1))
          }
        }
        buffer.append((("[EOW]", "[EOW]"), 1))
      }
      buffer
    }.reduceByKey(_ + _).map { tup =>
      tup._1._1 + "\t" + tup._1._2 + "\t" + tup._2
    }.saveAsTextFile(_cooccurFile)
  }

  def genIndex(_sc: SparkContext,
               _cooccurFile: String,
               _indexFile: String,
               _countFile: String,
               _indexedFile: String,
               _threshold: Int): Unit = {
    //load emoji
    val emojiArray = EmojiConst.emojiIndex.map{_._1}.toArray
    val emojiSet = emojiArray.toSet

    // Generate count file
    _sc.textFile(_cooccurFile + "/*").map { line =>
      val tuple = line.split("\t")
      val count = tuple(2).toInt
      (tuple(0), tuple(1), count)
    }.filter { tup =>
      tup._1 == tup._2 & (tup._3 >= _threshold | emojiSet(tup._1))
//      tup._1 == tup._2
    }.map { tup =>
      (tup._2, tup._3)
    }.sortBy(_._2, ascending = false, 1).map{
      tuple => tuple._1 + "\t" + tuple._2
    }.saveAsTextFile(_countFile)

    //load count file as map
    val countMap = _sc.textFile(_countFile + "/*").map{ line =>
      val tup = line.split("\t")
      (tup(0), tup(1).toInt)
    }.collectAsMap

    //load count file
    val wordArray = _sc.textFile(_countFile + "/*").map{line =>
      val tup = line.split("\t")
      (tup(0), tup(1).toInt)
    }.filter{tup =>
      !emojiSet(tup._1) & tup._1 != "[EOW]"
    }.sortBy(_._2, ascending = false, 1).map{_._1}.collect

    //gen index
    var cnt = 0
    val indexBuffer = new ListBuffer[(String, Int)]
    for (word <- emojiArray ++ wordArray) {
      indexBuffer.append((word, cnt))
      cnt += 1
    }
    _sc.parallelize(indexBuffer).map{tup =>
      tup._1 + "\t" +tup._2 + "\t" + countMap.getOrElse(tup._1, 0)
    }.saveAsTextFile(_indexFile)

    val indexMap = indexBuffer.toMap

    //index the cooccurrence files
    _sc.textFile(_cooccurFile + "/*").map { line =>
      val tuple = line.split("\t")
      val index1 = indexMap.getOrElse(tuple(0), 0)
      val index2 = indexMap.getOrElse(tuple(1), 0)
      val cooccur = tuple(2).toInt
      (index1, index2, cooccur)
    }.filter { tup =>
      tup._1 > 0 & tup._2 > 0
    }.map { tup =>
      tup._1 + "\t" + tup._2 + "\t" + tup._3
    }.saveAsTextFile(_indexedFile)
  }

  def parseArgs(map: Map[String, String], list: List[String]): Map[String, String] = {
    list match {
      case Nil => map
      case "-word-file" :: value :: tail =>
        parseArgs(map ++ Map(("wordFile", value)), tail)
      case "-index-file" :: value :: tail =>
        parseArgs(map ++ Map(("indexFile", value)), tail)
      case "-count-file" :: value :: tail =>
        parseArgs(map ++ Map(("countFile", value)), tail)
      case "-cooccur-file" :: value :: tail =>
        parseArgs(map ++ Map(("cooccurFile", value)), tail)
      case "-cooccur-indexed-file":: value:: tail =>
        parseArgs(map ++ Map(("indexedFile", value)), tail)
      case "-window" :: value :: tail =>
        parseArgs(map ++ Map(("window", value)), tail)
      case "-threshold" :: value :: tail =>
        parseArgs(map ++ Map(("threshold", value)), tail)

      case "-gen-cooccur" :: tail =>
        parseArgs(map ++ Map(("genCooccur", "1")), tail)
      case "-gen-index" :: tail =>
        parseArgs(map ++ Map(("genIndex", "1")), tail)

      case opt :: tail =>
        println("unknown option:" + opt)
        parseArgs(map, tail)
    }
  }

  def main(args: Array[String]): Unit = {
    println("Starting, parsing args")
    println(args)

    val argMap = parseArgs(Map[String, String](), args.toList)

    println(argMap)

    val sc = new SparkContext(new SparkConf())

    val wordFile = argMap.getOrElse("wordFile", "s3://beidaemr/aiwei/preprocessed/*")
    val indexFile = argMap.getOrElse("indexFile", "s3://beidaemr/aiwei/v2/win_index")
    val countFile = argMap.getOrElse("countFile", "s3://beidaemr/aiwei/v2/win_count")
    val cooccurRawFile = argMap.getOrElse("cooccurFile", "s3://beidaemr/aiwei/v2/win_cooccur_raw")
    val cooccurIndexedFile = argMap.getOrElse("indexedFile", "s3://beidaemr/aiwei/v2/win_cooccur_indexed")
    val window = argMap.getOrElse("window", "5").toInt
    val threshold = argMap.getOrElse("threshold", "5").toInt

    if (argMap.contains("genCooccur")){
      genCooccur(sc, wordFile, cooccurRawFile, window)
    }
    if (argMap.contains("genIndex")) {
      genIndex(sc, cooccurRawFile, indexFile, countFile, cooccurIndexedFile, threshold)
    }
    sc.stop()
  }
}
