import java.io.{File, PrintWriter}

import scala.collection.immutable.Map
import scala.collection.mutable
import scala.io.Source

/**
  * Created by aiwei on 9/22/16.
  */
object GenEmojiStats {
  def parseArgs(map: Map[String, String], list: List[String]): Map[String, String] = {
    list match {
      case Nil => map

      case "-dist-file" :: value :: tail =>
        parseArgs(map ++ Map(("distFile", value)), tail)
      case "-freq-file" :: value :: tail =>
        parseArgs(map ++ Map(("freqFile", value)), tail)
      case "-k" :: value :: tail =>
        parseArgs(map ++ Map(("kNearest", value)), tail)
      case "-embedding" :: value :: tail =>
        parseArgs(map ++ Map(("embedding", value)), tail)

      case opt :: tail =>
        println("unknown option:" + opt)
        parseArgs(map, tail)
    }
  }

  def main(args: Array[String]): Unit = {
    val argMap = parseArgs(Map[String, String](), args.toList)

    val distFile = argMap.getOrElse("distFile", "svr/emoji_stats/dist/win_log_line2")
    val freqFile = argMap.getOrElse("freqFile", "svr/emoji_stats/freq/win_log_line2")
    val embedding = argMap.getOrElse("embedding", "win_log_line2")
    val k = argMap.getOrElse("kNearest", "20").toInt
    val indexFile = "svr/index/msg"


    var freqSet = mutable.HashSet[Int]()
    var writer = new PrintWriter(new File(distFile))

    writer.write("emoji_id\tneighbor_id\tneighbor_type\tdistance\n")

    List("all", "word", "emoticon", "emoji").flatMap{ indexType =>
      Source.fromFile("svr/k_nearest/%s/%s".format(indexType, embedding)).getLines().map{line =>
        val tup = line.split("\t")
        (tup(0).toInt, (tup(1).toInt, tup(2).toDouble))
      }.filter(tup => tup._1 < 1281 & tup._2._2 > 0).toArray.groupBy(_._1).par.flatMap{
        case (emoji:Int, neighbors:Array[(Int, (Int, Double))]) =>
          freqSet += emoji
          neighbors.map{_._2}.sortBy{_._2}.take(k).map{case (neighbor, dist) =>
            freqSet += neighbor
            (emoji, neighbor, indexType, dist)
          }
      }
    }.foreach{case (emoji: Int, neighbor:Int, indexType:String, dist:Double) =>
        writer.write("%d\t%d\t%s\t%f\n".format(emoji, neighbor, indexType, dist))
    }

    writer.close()

    writer = new PrintWriter(new File(freqFile))
    writer.write("token_id\tcount\n")
    Source.fromFile(indexFile).getLines().map { line =>
      val tup = line.split("\t")
      (tup(0), tup(1).toInt, tup(2).toInt)
    }.filter{tup => freqSet(tup._2)}.foreach { case (word, idx, cnt) =>
      writer.write("%d\t%d\n".format(idx, cnt))
    }

    writer.close()
  }
}
