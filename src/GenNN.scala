import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.immutable.{Map, Vector}

/**
  * Created by aiwei on 9/7/16.
  */
object GenNN {
  def getSqrDist(vec1: Vector[Double], vec2: Vector[Double]): Double = {
    vec1.zip(vec2).map(tup => math.pow(tup._1 - tup._2, 2)).sum
  }

  def parseArgs(map: Map[String, String], list: List[String]): Map[String, String] = {
    list match {
      case Nil => map

      case "-out-file" :: value :: tail =>
        parseArgs(map ++ Map(("outFile", value)), tail)
      case "-k-nearest" :: value :: tail =>
        parseArgs(map ++ Map(("kNearest", value)), tail)
      case "-embedding" :: value :: tail =>
        parseArgs(map ++ Map(("embedding", value)), tail)
      case "-word-index" :: value :: tail =>
        parseArgs(map ++ Map(("wordIndex", value)), tail)
      case "-select" :: value :: tail =>
        parseArgs(map ++ Map(("select", value)), tail)

      case opt :: tail =>
        println("unknown option:" + opt)
        parseArgs(map, tail)
    }
  }

  def main(args: Array[String]): Unit = {
    val timestamp = System.currentTimeMillis() / 1000

    val argMap = parseArgs(Map[String, String](), args.toList)
    val sc = new SparkContext(new SparkConf())

    val embeddingFile = argMap.getOrElse("embedding", "")
    val outFile = argMap.getOrElse("outFile", "")
    val kNearest = argMap.getOrElse("kNearest", "50").toInt
    val wordIndex = argMap.getOrElse("wordIndex", "")
    val select = argMap.getOrElse("select", "")

    val embeddings = sc.textFile(embeddingFile, minPartitions = 40).distinct().map{ line =>
      val lst = line.split(" ").toList
      (lst.head.toInt, lst.drop(1).map(_.toDouble).toVector)
    }.cache()

    var candidates = embeddings

    if (wordIndex.length > 0){
      candidates = sc.textFile(wordIndex).distinct().map { line =>
        val tup = line.split("\t")
        (tup(0).toInt, tup(2))
      }.filter{case (idx, tokenType) =>
        select == "" | tokenType == select
      }.join(embeddings).map{case (idx, (tokenType, vec)) =>(idx, vec)}.cache()
    }

    candidates.map { case (idx:Int, vec:Vector[Double]) =>
      idx + " " + vec.mkString(" ")
    }.saveAsTextFile("s3://beidaemr/aiwei/tmp/tmp-" + timestamp)
    candidates = sc.textFile("s3://beidaemr/aiwei/tmp/tmp-" + timestamp + "/*").distinct().map{line =>
      val lst = line.split(" ").toList
      (lst.head.toInt, lst.drop(1).map(_.toDouble).toVector)
    }

    embeddings.cartesian(candidates).map{
      case ((wordIndex1: Int, vec1:Vector[Double]),(wordIndex2: Int, vec2:Vector[Double])) =>
        (wordIndex1, (wordIndex2, getSqrDist(vec1, vec2)))
    }.filter(tup => tup._1 != tup._2._1).groupByKey().mapValues{ list =>
      if (wordIndex == "") {
        list.toArray.sortBy(_._2).slice(1,kNearest + 1)
      } else {
        list.toArray.sortBy(_._2).slice(0,kNearest)
      }
    }.flatMap{ case (wordIndex1: Int, lst:Array[(Int, Double)]) =>
      lst.map{ case(wordIndex2, dist) =>
        wordIndex1 + "\t" + wordIndex2 + "\t" + dist
      }
    }.saveAsTextFile(outFile)

    sc.stop()
  }
}
