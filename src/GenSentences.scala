import java.util.regex.Pattern

import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.immutable.Map
import collection.JavaConverters._

/**
  * Created by aiwei on 9/20/16.
  */
object GenSentences {

  val pat = Pattern.compile(EmojiConst.emojiRegex)

  def genSentences(_sc: SparkContext,
                   _wordFile: String,
                   _outFile: String
                  ): Unit = {
    _sc.textFile(_wordFile).distinct().filter {
      _.split("\t").length > 3
    }.map { line =>
      val content = line.split("\t")(3)
      val tokens = Twokenize.tokenize(line.split("\t")(3)).asScala.toArray
      tokens.mkString(" ").toString
    }.saveAsTextFile(_outFile)
  }

  def parseArgs(map: Map[String, String], list: List[String]): Map[String, String] = {
    list match {
      case Nil => map
      case "-word-file" :: value :: tail =>
        parseArgs(map ++ Map(("wordFile", value)), tail)
      case "-out-file" :: value :: tail =>
        parseArgs(map ++ Map(("outFile", value)), tail)

      case opt :: tail =>
        println("unknown option:" + opt)
        parseArgs(map, tail)
    }
  }

  def main(args: Array[String]): Unit = {
    println("Starting, parsing args")
    println(args)

    val argMap = parseArgs(Map[String, String](), args.toList)

    println(argMap)

    val sc = new SparkContext(new SparkConf())

    val wordFile = argMap.getOrElse("wordFile", "s3://beidaemr/aiwei/preprocessed/*")
    val outFile = argMap.getOrElse("outFile", "s3://beidaemr/aiwei/sentences")

    genSentences(_sc = sc, _wordFile = wordFile, _outFile = outFile)

    sc.stop()
  }
}
