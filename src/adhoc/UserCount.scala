package adhoc

import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by aiwei on 10/19/16.
  */
object UserCount {
  def main(args: Array[String]): Unit = {
    val midFile = "s3://beidaemr/aiwei/preprocessed"
    val outFile = "s3://beidaemr/aiwei/adhoc/UserCount"
    val sc = new SparkContext(new SparkConf())
    val uniqUsers = sc.textFile(midFile).distinct().map{_.split("\t")(0)}.distinct.count
    sc.parallelize(Array(uniqUsers)).saveAsTextFile(outFile)
  }
}
