package v1

import java.util.Properties
import java.util.regex.Pattern

import edu.stanford.nlp.ling.CoreAnnotations.{SentencesAnnotation, TextAnnotation, TokensAnnotation}
import edu.stanford.nlp.pipeline.{Annotation, StanfordCoreNLP}
import collection.JavaConverters._

/**
  * Created by aiwei on 9/20/16.
  */
object Tokenizer {
  val pat = Pattern.compile(EmojiConst.emojiRegex)
  val props = new Properties()
  props.setProperty("annotators", "tokenize, ssplit")
  val coreNLP = new StanfordCoreNLP(props)

  def tokenize(ln: String, eof: Boolean = false): Array[String]= {
    val doc = new Annotation(ln)
    coreNLP.annotate(doc)
    val tokens = doc.get(classOf[SentencesAnnotation]).asScala.flatMap { sent =>
      sent.get(classOf[TokensAnnotation]).asScala.map { token =>
        (token.get(classOf[TextAnnotation]).toLowerCase, token.beginPosition())
      }
    }
    val matcher = pat.matcher(new String(ln.getBytes("UTF-8"), "UTF-8"))
    while (matcher.find()){
      tokens += ((matcher.group(), matcher.start()))
    }
    if (eof) {
      tokens += (("<EOF>", ln.length()))
    }
    tokens.toArray.sortBy(_._2).map{ tup => tup._1}
  }
}
