package v1

import java.io.{File, PrintWriter}
import java.util.regex.Pattern

import scala.collection.immutable.Map
import scala.io.Source

/**
  * Created by aiwei on 9/20/16.
  */
object LineResultFilter {
  def parseArgs(map: Map[String, String], list: List[String]): Map[String, String] = {
    list match {
      case Nil => map

      case "-in-file" :: value :: tail =>
        parseArgs(map ++ Map(("inFile", value)), tail)
      case "-word-file" :: value :: tail =>
        parseArgs(map ++ Map(("wordFile", value)), tail)
      case "-threshold" :: value :: tail =>
        parseArgs(map ++ Map(("threshold", value)), tail)
      case "-count-file" :: value :: tail =>
        parseArgs(map ++ Map(("countFile", value)), tail)
      case "-index-file" :: value :: tail =>
        parseArgs(map ++ Map(("indexFile", value)), tail)
      case "-emoji-file" :: value :: tail =>
        parseArgs(map ++ Map(("emojiFile", value)), tail)

      case opt :: tail =>
        println("unknown option:" + opt)
        parseArgs(map, tail)
    }
  }

  def main(args: Array[String]): Unit = {
    val argMap = parseArgs(Map[String, String](), args.toList)
    val inFile = argMap.getOrElse("inFile", "")
    val wordFile = argMap.getOrElse("wordFile", "")
    val countFile = argMap.getOrElse("countFile", "")
    val indexFile = argMap.getOrElse("indexFile", "")
    val emojiFile = argMap.getOrElse("emojiFile", "")
    val threshold = argMap.getOrElse("threshold", "1000").toInt

    val pat = Pattern.compile("^[a-zA-Z@#-']+$")

    val validWordSet = Source.fromFile(countFile).getLines.filter{ line =>
      val tup = line.split("\t")
      pat.matcher(tup(0)).find() & tup(1).toInt >= threshold
    }.map {_.split("\t")(0)}.toSet

    val validIndexSet = Source.fromFile(indexFile).getLines.filter{ line =>
      val tup = line.split("\t")
      val word = tup(0)
      validWordSet.contains(word)
    }.map{ _.split("\t")(1)}.toSet

    val emojiWriter = new PrintWriter(new File(emojiFile))
    val wordWriter = new PrintWriter(new File(wordFile))

    Source.fromFile(inFile).getLines.foreach { line =>
      val tup = line.split(" ")
      if (tup.length > 2) {
        val wordId = tup(0).toInt
        if (wordId <= 1281) {
          emojiWriter.write(line + "\n")
        } else if (validIndexSet.contains(tup(0))) {
          wordWriter.write(line + "\n")
        }
      }
    }
    emojiWriter.close
    wordWriter.close
  }

}
