package v1

import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.immutable.Map

/**
  * Created by aiwei on 9/20/16.
  */
object GenerateNearestNeighbor {
  def parseArgs(map : Map[String, String], list: List[String]) : Map[String, String] = {
    list match {
      case Nil => map

      case "-index-file" :: value :: tail =>
        parseArgs(map ++ Map(("indexFile", value)), tail)
      case "-count-file" :: value :: tail =>
        parseArgs(map ++ Map(("countFile", value)), tail)
      case "-embedding-file" :: value :: tail =>
        parseArgs(map ++ Map(("embeddingFile", value)), tail)
      case "-output" :: value :: tail =>
        parseArgs(map ++ Map(("outputFile", value)), tail)
      case "-min-count" :: value :: tail =>
        parseArgs(map ++ Map(("minCount", value)), tail)
      case "-k-nearest" :: value :: tail =>
        parseArgs(map ++ Map(("kNearest", value)), tail)

      case opt :: tail =>
        println("unknown option:" + opt)
        parseArgs(map, tail)
    }
  }

  def main(args: Array[String]) {
    val timestamp = System.currentTimeMillis() / 1000
    val emojiDescriptionFileName = "data/emojis"
    val emojiVecTempFileName = "tmp/emojiVec" + timestamp
    val wordVecTempFileName = "tmp/wordVec" + timestamp

    val argMap = parseArgs(Map[String, String](), args.toList)
    val indexFileName = argMap.getOrElse("indexFile", "") //data/wordindex"
    val countFileName = argMap.getOrElse("countFile", "") //"data/wordcount"
    val embeddingFileName = argMap.getOrElse("embeddingFile", "") //"output/line_log.txt"
    val outputFileName = argMap.getOrElse("outputFile", "") //"output/nn_log_filter1000_top50"
    val minCount = argMap.getOrElse("minCount", "500").toInt
    val kNearest = argMap.getOrElse("kNearest", "50").toInt

    if (indexFileName == "" | countFileName == "" | embeddingFileName == "" | outputFileName == "") {
      println("Please check parameter!")
      return
    }
    val sc = new SparkContext(new SparkConf())

    val idx = sc.textFile(indexFileName).distinct().map { line =>
      val tup = line.split("\t")
      (tup(1).toInt, tup(0))
      //  (index, word)
    }.collect().toMap

    val wordCount = sc.textFile(countFileName).distinct().map { line =>
      val tup = line.split("\t")
      (tup(0), tup(1).toInt)
    }.collect().toMap
    val idxcnt = idx.map { tup => (tup._1, wordCount.getOrElse(tup._2, 0)) }

    val vec = sc.textFile(embeddingFileName).distinct()

    vec.map { line =>
      val tup = line.split(" ")
      (tup(0), tup.length, line)
    }.filter { case (id, len, line) =>
      id.toInt < 1281 & len >2
    }.map {
      _._3
    }.saveAsTextFile(emojiVecTempFileName)

    vec.map { line =>
      val tup = line.split(" ")
      val id = tup(0)
      (id.toInt, tup.length, line)
    }.filter {
      case (id, len, line) => id.toInt > 1281 & idxcnt.getOrElse(id, 0) > minCount & len > 2
    }.map {
      _._3
    }.saveAsTextFile(wordVecTempFileName)

    val emojiVecs = sc.textFile(emojiVecTempFileName).map { line =>
      val tup = line.split(" ")
      val vec = tup.drop(1).map {
        _.toDouble
      }
      (tup(0).toInt, Vectors.dense(vec))
      //  (index, vector)
    }.cache()

    val wordVecs = sc.textFile(wordVecTempFileName).map { line =>
      val tup = line.split(" ")
      val vec = tup.drop(1).map {
        _.toDouble
      }
      (tup(0).toInt, Vectors.dense(vec))
      //  (index, vector)
    }.cache()

    val emojiDes = sc.textFile(emojiDescriptionFileName).distinct().map { line =>
      val tup = line.split("\t")
      (tup(0).toInt, (tup(1), tup(2)))
      //  (index, (char, description))
    }.map { tup =>
      (tup._1, (tup._2._1, wordCount.getOrElse(tup._2._1, -1), tup._2._2))
      //  (index, (char, count, description))
    }.filter { tup =>
      tup._2._2 > 0
    }.collect().toMap

    emojiVecs.cartesian(wordVecs).map { case ((emojiId, emojiVec), (wordId, wordVec)) =>
      (emojiId, (wordId, Vectors.sqdist(emojiVec, wordVec)))
    }.groupByKey.mapValues(wordList =>
      wordList.toList.sortBy(_._2).take(kNearest)
    ).map { case (id, lst) =>
      val txt = lst.map { tup =>
        "<%s, %s, %s>".format(
          idx.getOrElse(tup._1, "UNKNOWN"),
          idxcnt.getOrElse(tup._1, 0),
          tup._2)
      }.mkString(" ")
      val des = emojiDes.getOrElse(id, ("UNKNOWN", 0, "UNKNOWN"))
      id + "\t" + des._1 + "\t" + des._2 + "\t" + des._3 + "\t" + txt
    }.saveAsTextFile(outputFileName)

    sc.stop()

  }
}
