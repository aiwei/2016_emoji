package v1

import java.util.regex.Pattern

import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.immutable.Map
import scala.collection.mutable.ListBuffer
import scala.math._

/**
  * Created by aiwei on 9/20/16.
  */
object GenCooccur {

  val pat = Pattern.compile(EmojiConst.emojiRegex)

  def genCooccur(_sc: SparkContext,
                 _metaFile: String,
                 _wordFile: String,
                 _cooccurFile: String,
                 _window: Int): Unit = {
    val meta = _sc.textFile(_metaFile).distinct().filter { x =>
      val a = x.split("\t")
      a.length > 6 && a(0).length() == 32 && a(6) == "us"
    }.map { x =>
      val a = x.split("\t")
      (a(0), 1)
    }

    _sc.textFile(_wordFile).filter { x =>
      val a = x.split("\t")
      a.length > 2 && a(2).length() == 32
    }.map { line =>
      val tup = line.split("\t")
      (tup(2), tup(1))
    }.join(meta).map {
      case (duid: String, (text: String, 1)) => text
    }.flatMap { content =>
      val buffer = new ListBuffer[((String, String), Int)]
      val tokens = Tokenizer.tokenize(content, eof = false)
      val length = tokens.length
      val window = if (_window == 0) length else min(_window, length)

      for (i <- 0 to length - window) {
        var wordSet = Set[String]()
        for (j <- i until i + window) {
          val tk = tokens(j)
          if (!wordSet(tk)) {
            for (k <- wordSet) {
              buffer.append(((tk, k), 1))
              buffer.append(((k, tk), 1))
            }
            wordSet += tk
            buffer.append(((tk, tk), 1))
          }
        }
        buffer.append((("[EOW]", "[EOW]"), 1))
      }
      buffer
    }.reduceByKey(_ + _).map { tup =>
      tup._1._1 + "\t" + tup._1._2 + "\t" + tup._2
    }.saveAsTextFile(_cooccurFile)
  }

// TODO get rid of flatMap as it is super slow.

  def genIndex(_sc: SparkContext,
               _cooccurFile: String,
               _indexFile: String,
               _countFile: String,
               _indexedFile: String,
               _threshold: Int): Unit = {
    // Generate count file
    _sc.textFile(_cooccurFile + "/*").flatMap { line =>
      val tuple = line.split("\t")
      val count = tuple(2).toInt
      if (tuple(0) == tuple(1) & count >= _threshold) Some((tuple(0), count)) else None
    }.sortBy(_._2, ascending = false, 1).map{
      tuple => tuple._1 + "\t" + tuple._2
    }.saveAsTextFile(_countFile)

    //load emoji
    val emojiArray = EmojiConst.emojiIndex.map{_._1}.toArray
    val emojiSet = emojiArray.toSet

    //load count file
    val wordArray = _sc.textFile(_countFile + "/*").map{line =>
      val tup = line.split("\t")
      (tup(0), tup(1).toInt)
    }.filter{tup =>
      !emojiSet(tup._1) & tup._1 != "[EOW]"
    }.sortBy(_._2, ascending = false, 1).map{_._1}.collect

    //gen index
    var cnt = 1
    val indexBuffer = new ListBuffer[(String, Int)]
    for (word <- emojiArray ++ wordArray) {
      indexBuffer.append((word, cnt))
      cnt += 1
    }
    _sc.parallelize(indexBuffer).map{tup =>
      tup._1 + "\t" +tup._2
    }.saveAsTextFile(_indexFile)
    val indexMap = indexBuffer.toMap

    //index the cooccurrence files
    _sc.textFile(_cooccurFile + "/*").flatMap { line =>
      val tuple = line.split("\t")
      val index1 = indexMap.getOrElse(tuple(0), 0)
      val index2 = indexMap.getOrElse(tuple(1), 0)
      val cooccur = tuple(2).toInt
      if (index1 > 0 & index2 > 0) Some(index1 + "\t" + index2 + "\t" + cooccur) else None
    }.saveAsTextFile(_indexedFile)
  }

  def parseArgs(map: Map[String, String], list: List[String]): Map[String, String] = {
    list match {
      case Nil => map
      case "-word-file" :: value :: tail =>
        parseArgs(map ++ Map(("wordFile", value)), tail)
      case "-meta-file" :: value :: tail =>
        parseArgs(map ++ Map(("metaFile", value)), tail)
      case "-index-file" :: value :: tail =>
        parseArgs(map ++ Map(("indexFile", value)), tail)
      case "-count-file" :: value :: tail =>
        parseArgs(map ++ Map(("countFile", value)), tail)
      case "-cooccur-file" :: value :: tail =>
        parseArgs(map ++ Map(("cooccurFile", value)), tail)
      case "-cooccur-indexed-file":: value:: tail =>
        parseArgs(map ++ Map(("indexedFile", value)), tail)
      case "-window" :: value :: tail =>
        parseArgs(map ++ Map(("window", value)), tail)
      case "-threshold" :: value :: tail =>
        parseArgs(map ++ Map(("threshold", value)), tail)

      case "-gen-cooccur" :: tail =>
        parseArgs(map ++ Map(("genCooccur", "1")), tail)
      case "-gen-index" :: tail =>
        parseArgs(map ++ Map(("genIndex", "1")), tail)

      case opt :: tail =>
        println("unknown option:" + opt)
        parseArgs(map, tail)
    }
  }

  def main(args: Array[String]): Unit = {
    println("Starting, parsing args")
    println(args)

    val argMap = parseArgs(Map[String, String](), args.toList)

    println(argMap)

    val sc = new SparkContext(new SparkConf())

    val metaFile = argMap.getOrElse("metaFile", "s3://beidas3/kikakeyboard/meta_full/20150930/")
    val wordFile = argMap.getOrElse("wordFile", "s3://beidas3/kikakeyboard/word/201509*/language=en_US/*")
    val indexFile = argMap.getOrElse("indexFile", "s3://beidaemr/aiwei/embedding/index")
    val countFile = argMap.getOrElse("countFile", "s3://beidaemr/aiwei/embedding/count")
    val cooccurRawFile = argMap.getOrElse("cooccurFile", "s3://beidaemr/aiwei/embedding/cooccur_raw")
    val cooccurIndexedFile = argMap.getOrElse("indexedFile", "s3://beidaemr/aiwei/embedding/cooccur_indexed")
    val window = argMap.getOrElse("window", "0").toInt
    val threshold = argMap.getOrElse("threshold", "0").toInt

    if (argMap.contains("genCooccur")){
      genCooccur(sc, metaFile, wordFile, cooccurRawFile, window)
    }
    if (argMap.contains("genIndex")) {
      genIndex(sc, cooccurRawFile, indexFile, countFile, cooccurIndexedFile, threshold)
    }
    sc.stop()
  }
}
