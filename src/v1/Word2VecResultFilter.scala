package v1

import java.io.{File, PrintWriter}
import java.nio.charset.CodingErrorAction
import java.util.regex.Pattern

import scala.collection.immutable.Map
import scala.io.{Codec, Source}

/**
  * Created by aiwei on 9/20/16.
  */
object Word2VecResultFilter {
  def parseArgs(map: Map[String, String], list: List[String]): Map[String, String] = {
    list match {
      case Nil => map

      case "-in-file" :: value :: tail =>
        parseArgs(map ++ Map(("inFile", value)), tail)
      case "-word-file" :: value :: tail =>
        parseArgs(map ++ Map(("wordFile", value)), tail)
      case "-threshold" :: value :: tail =>
        parseArgs(map ++ Map(("threshold", value)), tail)
      case "-count-file" :: value :: tail =>
        parseArgs(map ++ Map(("countFile", value)), tail)
      case "-index-file" :: value :: tail =>
        parseArgs(map ++ Map(("indexFile", value)), tail)
      case "-emoji-file" :: value :: tail =>
        parseArgs(map ++ Map(("emojiFile", value)), tail)

      case opt :: tail =>
        println("unknown option:" + opt)
        parseArgs(map, tail)
    }
  }

  def main(args: Array[String]): Unit = {
    val argMap = parseArgs(Map[String, String](), args.toList)
    val inFile = argMap.getOrElse("inFile", "")
    val wordFile = argMap.getOrElse("wordFile", "")
    val countFile = argMap.getOrElse("countFile", "")
    val indexFile = argMap.getOrElse("indexFile", "")
    val emojiFile = argMap.getOrElse("emojiFile", "")
    val threshold = argMap.getOrElse("threshold", "1000").toInt

    val pat = Pattern.compile("^[a-zA-Z@#-']+$")

    val validWordSet = Source.fromFile(countFile).getLines.filter{ line =>
      val tup = line.split("\t")
      pat.matcher(tup(0)).find() & tup(1).toInt >= threshold
    }.map {_.split("\t")(0)}.toSet

    val indexMap = Source.fromFile(indexFile).getLines.map{ line =>
      val tup = line.split("\t")
      (tup(0), tup(1))
    }.toMap

    val emojiWriter = new PrintWriter(new File(emojiFile))
    val wordWriter = new PrintWriter(new File(wordFile))

    val decoder = Codec.UTF8.decoder.onMalformedInput(CodingErrorAction.IGNORE)

    Source.fromFile(inFile)(decoder).getLines.foreach { line =>
      val tup = line.split(" ")
      if (tup.length > 2) {
        indexMap.get(tup(0)) match {
          case Some(wordId) =>
            if (wordId.toInt <= 1281) {
              emojiWriter.write(wordId + " " + tup.drop(1).mkString(" ") + "\n")
            } else if (validWordSet.contains(tup(0))) {
              wordWriter.write(wordId + " " + tup.drop(1).mkString(" ") + "\n")
            }
          case _ =>
        }

      }
    }
    emojiWriter.close()
    wordWriter.close()
  }

}
