package v1

import java.io.{File, PrintWriter}

import scala.collection.immutable.{Map, Vector}
import scala.collection.parallel.ForkJoinTaskSupport
import scala.collection.parallel.immutable.ParMap
import scala.collection.parallel.mutable.ParArray
import scala.concurrent.forkjoin.ForkJoinPool
import scala.io.Source
import scala.util.Random

/**
  * Created by aiwei on 9/20/16.
  */
object GenNN {

  type KnnMap = ParMap[Word, Array[Word]]

  def genRPTree(words: Array[Word], threshold: Int, rand: Random): Tree = {
    val len = words.length
    if (words.length <= threshold) {
      return Leaf(words)
    }
    val rand1 = rand.nextInt(len)
    val rand2 = (rand.nextInt(len - 1) + rand1 + 1) % len
    val p1: Word = words(rand1)
    val p2: Word = words(rand2)

    // the plane is defined by " ax - b = 0 "
    val a = p1.vec.zip(p2.vec).map(tup => tup._1 - tup._2)
    val b = (p1.vec.map(math.pow(_, 2)).sum - p2.vec.map(math.pow(_, 2)).sum) / 2
    val plane = new Plane(a, b)

    val posWords = words.filter { word => plane.getSign(word.vec) == 1 }
    val negWords = words.filter { word => plane.getSign(word.vec) == -1 }
    new Node(plane, genRPTree(posWords, threshold, rand), genRPTree(negWords, threshold, rand))
  }

  def getLeaf(vec: Vector[Double], root: Tree): Array[Word] = root match {
    case Leaf(words) => words
    case Node(plane, posNode, negNode) =>
      plane.getSign(vec) match {
        case 1 => getLeaf(vec, posNode)
        case -1 => getLeaf(vec, negNode)
      }
  }

  def parseArgs(map: Map[String, String], list: List[String]): Map[String, String] = {
    list match {
      case Nil => map

      case "-emoji-file" :: value :: tail =>
        parseArgs(map ++ Map(("emojiFile", value)), tail)
      case "-word-file" :: value :: tail =>
        parseArgs(map ++ Map(("wordFile", value)), tail)
      case "-out-file" :: value :: tail =>
        parseArgs(map ++ Map(("outFile", value)), tail)
      case "-n-tree" :: value :: tail =>
        parseArgs(map ++ Map(("nTree", value)), tail)
      case "-leaf-threshold" :: value :: tail =>
        parseArgs(map ++ Map(("leafThreshold", value)), tail)
      case "-count-file" :: value :: tail =>
        parseArgs(map ++ Map(("countFile", value)), tail)
      case "-n-iteration" :: value :: tail =>
        parseArgs(map ++ Map(("nIteration", value)), tail)
      case "-k-nearest":: value :: tail =>
        parseArgs(map ++ Map(("kNearest", value)), tail)
      case "-n-thread" :: value :: tail =>
        parseArgs(map ++ Map(("nThread", value)), tail)

      case "-brutal" :: tail =>
        parseArgs(map ++ Map(("isBrutal", "1")), tail)

      case opt :: tail =>
        println("unknown option:" + opt)
        parseArgs(map, tail)
    }
  }

  def getSqrDist(vec1: Vector[Double], vec2: Vector[Double]): Double = {
    vec1.zip(vec2).map(tup => math.pow(tup._1 - tup._2, 2)).sum
  }

  def getInitKnn(words: ParArray[Word], trees: ParArray[Tree], kNearest: Int, nThread: Int): KnnMap = {
    words.tasksupport = new ForkJoinTaskSupport(new ForkJoinPool(nThread))
    words.map {word =>
      val initNn = trees.flatMap { getLeaf(word.vec, _) }.distinct.filter(_ != word).map { candidate =>
        (candidate, getSqrDist(word.vec, candidate.vec))
      }.seq.sortBy(_._2).take(kNearest).map{_._1}.toArray

      (word, initNn)
    }.toMap
  }

  def getNewKnn(words: ParArray[Word], oldKnn: KnnMap, kNearest: Int, nThread: Int): KnnMap = {
    words.tasksupport = new ForkJoinTaskSupport(new ForkJoinPool(nThread))
    words.map { word =>
      val oldNn = oldKnn.get(word) match{
        case Some(x) => x
        case None =>
          println("Weird! Cannot find word with index " + word.id)
          new Array[Word](0)
      }
      val newNn = oldNn.flatMap{ oldCandidate =>
        oldKnn.get(oldCandidate) match {
          case Some(x) => x
          case None =>
            println("Weird! Cannot find word with index " + oldCandidate.id)
            new Array[Word](0)
        }
      }.distinct.filter(_ != word).map { candidate =>
        (candidate, getSqrDist(word.vec, candidate.vec))
      }.sortBy(_._2).take(kNearest).map { _._1 }

      (word, newNn)
    }.toMap
  }

  def brutalForce(emojis: Array[Word], words: Array[Word], kNearest: Int, nThread: Int): KnnMap = {
    println("OK, let's do it brutal.")
    emojis.par.map{ emoji =>
      val nn = words.map{ word =>
        (word, getSqrDist(emoji.vec, word.vec))
      }.seq.sortBy(_._2).take(kNearest).map {_._1}.toArray
      (emoji, nn)
    }.toMap.par
  }

  def LargeVis(emojis: Array[Word], words: Array[Word], kNearest: Int, nThread: Int,
               nTree: Int, leafThreshold: Int, nIteration: Int): KnnMap = {
    val treeThreads = (1 to nTree).toParArray
    treeThreads.tasksupport = new ForkJoinTaskSupport(new ForkJoinPool(nThread))
    val trees = treeThreads.map(tid => genRPTree(words = words, threshold = leafThreshold, rand = new Random(tid)))
    println("finished generating RP trees")

    val allWords = (words ++ emojis).par
    var knn: KnnMap = getInitKnn(words = allWords, trees = trees, kNearest = kNearest, nThread = nThread)
    println("finished generating initial KNN")

    for (i <- 1 to nIteration) {
      knn = getNewKnn(words = allWords, oldKnn = knn, kNearest = kNearest, nThread = nThread)
      println("finished iteration " + i)
    }

    getNewKnn(words = emojis.par, oldKnn = knn, kNearest = kNearest, nThread = nThread)
  }

  def main(args: Array[String]): Unit = {
    val argMap = parseArgs(Map[String, String](), args.toList)

    val emojiFile = argMap.getOrElse("emojiFile", "output/line3/line_log_emoji")
    val wordFile = argMap.getOrElse("wordFile", "output/line3/line_log_word_filter2000")
    val outFile = argMap.getOrElse("outFile", "output/line3/line_log_nn")
    val nTree = argMap.getOrElse("nTree", "80").toInt
    val leafThreshold = argMap.getOrElse("leafThreshold", "20").toInt
    val kNearest = argMap.getOrElse("kNearest", "20").toInt
    val nIteration = argMap.getOrElse("nIteration", "2").toInt
    val nThread = argMap.getOrElse("nThread", "20").toInt
    val isBrutal = argMap.getOrElse("isBrutal", "0").toInt

    val words = Source.fromFile(wordFile).getLines.map { line =>
      val lst = line.split(" ").toList
      new Word(_id = lst.head.toInt, _vec = lst.drop(1).map(_.toDouble).toVector)
    }.toArray
    println("finished loading word vectors")

    val emojis = Source.fromFile(emojiFile).getLines.map { line =>
      val lst = line.split(" ").toList
      new Word(_id = lst.head.toInt, _vec = lst.drop(1).map(_.toDouble).toVector)
    }.toArray

    println("finished loading emoji vectors")


    val writer = new PrintWriter(new File(outFile))
    val emojiKnn = isBrutal match {
      case 1 => brutalForce(emojis = emojis, words = words, kNearest = kNearest, nThread = nThread)
      case _ => LargeVis(emojis = emojis, words = words, kNearest = kNearest, nThread = nThread,
        nTree = nTree, leafThreshold = leafThreshold, nIteration = nIteration)
    }

    emojiKnn.toArray.sortBy(_._1.id).foreach{ case (word, nnArray) =>
      writer.write(word.id.toString)
      nnArray.map{nn =>
        (nn.id, getSqrDist(word.vec, nn.vec))
      }.foreach{ case (nn, dist) =>
        writer.write("\t" + nn + " " + dist)
      }
      writer.write("\n")
    }

    writer.close()
  }

  abstract class Tree

  class Word(_id: Int, _vec: Vector[Double]) {
    val id = _id
    val vec = _vec
  }

  class Plane(a: Vector[Double], b: Double) {
    def getSign(vec: Vector[Double]): Int = {
      val sig = a.zip(vec).map(tup => tup._1 * tup._2).sum - b
      if (sig >= 0) 1 else -1
    }
  }

  case class Leaf(words: Array[Word]) extends Tree

  case class Node(plane: Plane, posNode: Tree, negNode: Tree) extends Tree
}
