package v1

import java.util.regex.Pattern

import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.immutable.Map

/**
  * Created by aiwei on 9/20/16.
  */
object GenSentences {

  val pat = Pattern.compile(EmojiConst.emojiRegex)

  def genSentences(_sc: SparkContext,
                 _metaFile: String,
                 _wordFile: String,
                 _outFile: String
                ): Unit = {
    val meta = _sc.textFile(_metaFile).distinct().filter { x =>
      val a = x.split("\t")
      a.length > 6 && a(0).length() == 32 && a(6) == "us"
    }.map { x =>
      val a = x.split("\t")
      (a(0), 1)
    }

    println("generating sentences")
    _sc.textFile(_wordFile).filter { x =>
      val a = x.split("\t")
      a.length > 2 && a(2).length() == 32
    }.map { line =>
      val tup = line.split("\t")
      (tup(2), tup(1))
//    }.map{
//      case (duid: String, text) => text
    }.join(meta).map {
      case (duid: String, (text: String, 1)) => text
    }.map { content =>
      Tokenizer.tokenize(content, eof = false).mkString(" ").toString
    }.saveAsTextFile(_outFile)
    println("finish generating sentences")
  }

  def parseArgs(map: Map[String, String], list: List[String]): Map[String, String] = {
    list match {
      case Nil => map
      case "-word-file" :: value :: tail =>
        parseArgs(map ++ Map(("wordFile", value)), tail)
      case "-meta-file" :: value :: tail =>
        parseArgs(map ++ Map(("metaFile", value)), tail)
      case "-out-file" :: value :: tail =>
        parseArgs(map ++ Map(("outFile", value)), tail)

      case opt :: tail =>
        println("unknown option:" + opt)
        parseArgs(map, tail)
    }
  }

  def main(args: Array[String]): Unit = {
    println("Starting, parsing args")
    println(args)

    val argMap = parseArgs(Map[String, String](), args.toList)

    println(argMap)

    val sc = new SparkContext(new SparkConf())

    val metaFile = argMap.getOrElse("metaFile", "s3://beidas3/kikakeyboard/meta_full/20150930/")
    val wordFile = argMap.getOrElse("wordFile", "s3://beidas3/kikakeyboard/word/201509*/language=en_US/*")
    val outFile = argMap.getOrElse("outFile", "s3://beidaemr/aiwei/sentences")

    genSentences(_sc = sc, _metaFile = metaFile, _wordFile = wordFile, _outFile = outFile)

    sc.stop()
  }
}
