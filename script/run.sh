#!/bin/bash

# PMI transform
#scala -J-Xmx16g src/CooccurrencePmiTransformer.scala \
# -in-file data/line2/wordcooccur \
# -count-file data/line2/wordcount -index-file data/line2/wordindex \
# -out-file data/line2/wordcooccur_pmi
#scala -J-Xmx16g src/CooccurrencePmiTransformer.scala \
# -in-file data/line3/wordcooccur \
# -count-file data/line3/wordcount -index-file data/line3/wordindex \
# -out-file data/line3/wordcooccur_pmi

# LINE
#LINE/linux/line -train data/line2/wordcooccur \
# -output output/line2/line_raw.out -samples 40000 -threads 20
#LINE/linux/line -train data/line2/wordcooccur_log \
# -output output/line2/line_log.out -samples 40000 -threads 20
#LINE/linux/line -train data/line2/wordcooccur_pmi \
# -output output/line2/line_pmi.out -samples 40000 -threads 20
#LINE/linux/line -train data/line3/wordcooccur \
# -output output/line3/line_raw.out -samples 40000 -threads 20
#LINE/linux/line -train data/line3/wordcooccur_log \
# -output output/line3/line_log.out -samples 40000 -threads 20
#LINE/linux/line -train data/line3/wordcooccur_pmi \
# -output output/line3/line_pmi.out -samples 40000 -threads 20


function filter_line_result() {
MSG_FILTER=$1
WIN_FILTER=$2
W2V_FILTER=$3

echo "filter msg line result"
scala -J-Xmx32g src/LineResultFilter.scala \
 -in-file output/line2/line_raw.out \
 -count-file data/line2/wordcount -index-file data/line2/wordindex \
 -emoji-file output/line2/line_raw_emoji \
 -word-file output/line2/line_raw_word_filter${MSG_FILTER} \
 -threshold ${MSG_FILTER}
scala -J-Xmx32g src/LineResultFilter.scala \
 -in-file output/line2/line_log.out \
 -count-file data/line2/wordcount -index-file data/line2/wordindex \
 -emoji-file output/line2/line_log_emoji \
 -word-file output/line2/line_log_word_filter${MSG_FILTER} \
 -threshold ${MSG_FILTER}
scala -J-Xmx32g src/LineResultFilter.scala \
 -in-file output/line2/line_pmi.out \
 -count-file data/line2/wordcount -index-file data/line2/wordindex \
 -emoji-file output/line2/line_pmi_emoji \
 -word-file output/line2/line_pmi_word_filter${MSG_FILTER} \
 -threshold ${MSG_FILTER}

echo "filter win line result"
scala -J-Xmx32g src/LineResultFilter.scala \
 -in-file output/line3/line_raw.out \
 -count-file data/line3/wordcount -index-file data/line3/wordindex \
 -emoji-file output/line3/line_raw_emoji \
 -word-file output/line3/line_raw_word_filter${WIN_FILTER} \
 -threshold ${WIN_FILTER}
scala -J-Xmx32g src/LineResultFilter.scala \
 -in-file output/line3/line_log.out \
 -count-file data/line3/wordcount -index-file data/line3/wordindex \
 -emoji-file output/line3/line_log_emoji \
 -word-file output/line3/line_log_word_filter${WIN_FILTER} \
 -threshold ${WIN_FILTER}
scala -J-Xmx32g src/LineResultFilter.scala \
 -in-file output/line3/line_pmi.out \
 -count-file data/line3/wordcount -index-file data/line3/wordindex \
 -emoji-file output/line3/line_pmi_emoji \
 -word-file output/line3/line_pmi_word_filter${WIN_FILTER} \
 -threshold ${WIN_FILTER}

echo "filter w2v result"
scala -J-Xmx32g src/Word2VecResultFilter.scala \
 -in-file output/word2vec/vectors.txt \
 -count-file data/line2/wordcount -index-file data/line2/wordindex \
 -emoji-file output/word2vec/w2v_emoji \
 -word-file output/word2vec/w2v_word_filter${W2V_FILTER} \
 -threshold ${W2V_FILTER}
}

# gen NN
function gen_nn() {
MSG_FILTER=$1
WIN_FILTER=$2
W2V_FILTER=$3

echo "gen msg nn"
scala -J-Xmx32g src/GenNN.scala \
 -emoji-file output/line2/line_raw_emoji \
 -word-file output/line2/line_raw_word_filter${MSG_FILTER} \
 -out-file output/line2/nn_raw_filter${MSG_FILTER} \
 -k-nearest 20 -n-thread 20 -brutal
scala -J-Xmx32g src/GenNN.scala \
 -emoji-file output/line2/line_log_emoji \
 -word-file output/line2/line_log_word_filter${MSG_FILTER} \
 -out-file output/line2/nn_log_filter${MSG_FILTER} \
 -k-nearest 20 -n-thread 20 -brutal
scala -J-Xmx32g src/GenNN.scala \
 -emoji-file output/line2/line_pmi_emoji \
 -word-file output/line2/line_pmi_word_filter${MSG_FILTER} \
 -out-file output/line2/nn_pmi_filter${MSG_FILTER} \
 -k-nearest 20 -n-thread 20 -brutal

echo "gen win nn"
scala -J-Xmx32g src/GenNN.scala \
 -emoji-file output/line3/line_raw_emoji \
 -word-file output/line3/line_raw_word_filter${WIN_FILTER} \
 -out-file output/line3/nn_raw_filter${WIN_FILTER} \
 -k-nearest 20 -n-thread 20 -brutal
scala -J-Xmx32g src/GenNN.scala \
 -emoji-file output/line3/line_log_emoji \
 -word-file output/line3/line_log_word_filter${WIN_FILTER} \
 -out-file output/line3/nn_log_filter${WIN_FILTER} \
 -k-nearest 20 -n-thread 20 -brutal
scala -J-Xmx32g src/GenNN.scala \
 -emoji-file output/line3/line_pmi_emoji \
 -word-file output/line3/line_pmi_word_filter${WIN_FILTER} \
 -out-file output/line3/nn_pmi_filter${WIN_FILTER} \
 -k-nearest 20 -n-thread 20 -brutal

echo "gen w2v nn"
scala -J-Xmx32g src/GenNN.scala \
 -emoji-file output/word2vec/w2v_emoji \
 -word-file output/word2vec/w2v_word_filter${W2V_FILTER} \
 -out-file output/word2vec/nn_filter${W2V_FILTER} \
 -k-nearest 20 -n-thread 20 -brutal
}

# gen html
function gen_html() {

MSG_FILTER=$1
WIN_FILTER=$2
W2V_FILTER=$3

echo "gen html"
python python/gen_html.py --input output/line2/nn_raw_filter${MSG_FILTER} \
 --index data/line2/wordindex --count data/line2/wordcount \
 --output output/line2/msg_raw_filter${MSG_FILTER}.html
python python/gen_html.py --input output/line2/nn_log_filter${MSG_FILTER} \
 --index data/line2/wordindex --count data/line2/wordcount \
 --output output/line2/msg_log_filter${MSG_FILTER}.html
python python/gen_html.py --input output/line2/nn_pmi_filter${MSG_FILTER} \
 --index data/line2/wordindex --count data/line2/wordcount \
 --output output/line2/msg_pmi_filter${MSG_FILTER}.html

python python/gen_html.py --input output/line3/nn_raw_filter${WIN_FILTER} \
 --index data/line3/wordindex --count data/line3/wordcount \
 --output output/line3/win_raw_filter${WIN_FILTER}.html
python python/gen_html.py --input output/line3/nn_log_filter${WIN_FILTER} \
 --index data/line3/wordindex --count data/line3/wordcount \
 --output output/line3/win_log_filter${WIN_FILTER}.html
python python/gen_html.py --input output/line3/nn_pmi_filter${WIN_FILTER} \
 --index data/line3/wordindex --count data/line3/wordcount \
 --output output/line3/win_pmi_filter${WIN_FILTER}.html

python python/gen_html.py --input output/word2vec/nn_filter${W2V_FILTER} \
 --index data/line2/wordindex --count data/line2/wordcount \
 --output output/word2vec/w2v_filter${W2V_FILTER}.html
}

#MSG_FILTER=2000
#WIN_FILTER=5000
#W2V_FILTER=2000

#filter_line_result ${MSG_FILTER} ${WIN_FILTER} ${W2V_FILTER}
#gen_nn ${MSG_FILTER} ${WIN_FILTER} ${W2V_FILTER}
#gen_html ${MSG_FILTER} ${WIN_FILTER} ${W2V_FILTER}

#MSG_FILTER=5000
#WIN_FILTER=10000
#W2V_FILTER=5000

#filter_line_result ${MSG_FILTER} ${WIN_FILTER} ${W2V_FILTER}
#gen_nn ${MSG_FILTER} ${WIN_FILTER} ${W2V_FILTER}
#gen_html ${MSG_FILTER} ${WIN_FILTER} ${W2V_FILTER}

python python/evaluation/pooling.py \
 -input_files \
  output/line2/nn_raw_filter2000 output/line2/nn_log_filter2000 output/line2/nn_log_filter2000 \
  output/line2/nn_raw_filter5000 output/line2/nn_raw_filter5000 output/line2/nn_raw_filter5000 \
  output/line3/nn_raw_filter5000 output/line3/nn_raw_filter5000 output/line3/nn_raw_filter5000 \
  output/line3/nn_raw_filter10000 output/line3/nn_log_filter10000 output/line3/nn_log_filter10000 \
  output/word2vec/nn_filter2000 output/word2vec/nn_filter5000 \
 -index_files \
  data/line2/wordindex data/line2/wordindex data/line2/wordindex \
  data/line2/wordindex data/line2/wordindex data/line2/wordindex \
  data/line3/wordindex data/line3/wordindex data/line3/wordindex \
  data/line3/wordindex data/line3/wordindex data/line3/wordindex \
  data/line2/wordindex data/line2/wordindex \
 -tags \
  msg_raw_filter2000 msg_log_filter2000 msg_pmi_filter2000 \
  msg_raw_filter5000 msg_log_filter5000 msg_pmi_filter5000 \
  win_raw_filter5000 win_log_filter5000 win_pmi_filter5000 \
  win_raw_filter10000 win_log_filter10000 win_pmi_filter10000 \
  w2v_filter2000 w2v_filter5000 \
 -output_dir output/trec_eval