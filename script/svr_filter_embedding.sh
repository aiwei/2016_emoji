#!/usr/bin/env bash

if [ ! -f svr/filtered_embedding/index ]
then
scala -cp svr/bin/v2.jar GenFilteredIndex
fi

# win
scala -cp svr/bin/v2.jar FilterEmbedding \
 -index-file svr/index/win \
 -in-file svr/embedding/win_log_line2 \
 -out-file svr/filtered_embedding/win_log_line2

scala -cp svr/bin/v2.jar FilterEmbedding \
 -index-file svr/index/win \
 -in-file svr/embedding/win_log_line1 \
 -out-file svr/filtered_embedding/win_log_line1

scala -cp svr/bin/v2.jar FilterEmbedding \
 -index-file svr/index/win \
 -in-file svr/embedding/win_raw_line2 \
 -out-file svr/filtered_embedding/win_raw_line2

scala -cp svr/bin/v2.jar FilterEmbedding \
 -index-file svr/index/win \
 -in-file svr/embedding/win_raw_line1 \
 -out-file svr/filtered_embedding/win_raw_line1

scala -cp svr/bin/v2.jar FilterEmbedding \
 -index-file svr/index/win \
 -in-file svr/embedding/win_pmi_line2 \
 -out-file svr/filtered_embedding/win_pmi_line2

scala -cp svr/bin/v2.jar FilterEmbedding \
 -index-file svr/index/win \
 -in-file svr/embedding/win_pmi_line1 \
 -out-file svr/filtered_embedding/win_pmi_line1

# msg
scala -cp svr/bin/v2.jar FilterEmbedding \
 -index-file svr/index/msg \
 -in-file svr/embedding/msg_log_line2 \
 -out-file svr/filtered_embedding/msg_log_line2

scala -cp svr/bin/v2.jar FilterEmbedding \
 -index-file svr/index/msg \
 -in-file svr/embedding/msg_log_line1 \
 -out-file svr/filtered_embedding/msg_log_line1

scala -cp svr/bin/v2.jar FilterEmbedding \
 -index-file svr/index/msg \
 -in-file svr/embedding/msg_raw_line2 \
 -out-file svr/filtered_embedding/msg_raw_line2

scala -cp svr/bin/v2.jar FilterEmbedding \
 -index-file svr/index/msg \
 -in-file svr/embedding/msg_raw_line1 \
 -out-file svr/filtered_embedding/msg_raw_line1

scala -cp svr/bin/v2.jar FilterEmbedding \
 -index-file svr/index/msg \
 -in-file svr/embedding/msg_pmi_line2 \
 -out-file svr/filtered_embedding/msg_pmi_line2

scala -cp svr/bin/v2.jar FilterEmbedding \
 -index-file svr/index/msg \
 -in-file svr/embedding/msg_pmi_line1 \
 -out-file svr/filtered_embedding/msg_pmi_line1

scala -cp svr/bin/v2.jar FilterWord2VecEmbedding \
 -in-file svr/embedding/w2v \
 -out-file svr/filtered_embedding/w2v
