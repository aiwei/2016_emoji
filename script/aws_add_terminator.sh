#!/usr/bin/env bash
aws emr add-steps \
--cluster-id j-2WI6BT2VTW3P1 \
--steps Type=Spark,Name='terminator',ActionOnFailure=TERMINATE_JOB_FLOW,Args="[--class,Terminator,s3://beidaemr/aiwei/jars/v2.jar]"