#!/usr/bin/env bash
aws emr add-steps \
--cluster-id j-181J8MUCWAF6J \
--steps Type=Spark,Name='preprocess',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,116, \
  --class,PreProcess,s3://beidaemr/aiwei/jars/v2.jar]"