#!/bin/bash
#python python/v2/gen_html.py -i svr/knn/win_raw -o svr/html/win_raw.html
#python python/v2/gen_html.py -i svr/knn/win_log -o svr/html/win_log.html
#python python/v2/gen_html.py -i svr/knn/win_pmi -o svr/html/win_pmi.html
#python python/v2/gen_html.py -i svr/knn/msg_raw -o svr/html/msg_raw.html
#python python/v2/gen_html.py -i svr/knn/msg_log -o svr/html/msg_log.html
#python python/v2/gen_html.py -i svr/knn/msg_pmi -o svr/html/msg_pmi.html
#python python/v2/gen_html.py -i svr/knn/w2v -o svr/html/w2v.html

#python python/v2/gen_html.py -i svr/knw/win_raw -o svr/html_knw/win_raw.html
#python python/v2/gen_html.py -i svr/knw/win_log -o svr/html_knw/win_log.html
#python python/v2/gen_html.py -i svr/knw/win_pmi -o svr/html_knw/win_pmi.html
#python python/v2/gen_html.py -i svr/knw/msg_raw -o svr/html_knw/msg_raw.html
#python python/v2/gen_html.py -i svr/knw/msg_log -o svr/html_knw/msg_log.html
#python python/v2/gen_html.py -i svr/knw/msg_pmi -o svr/html_knw/msg_pmi.html
#python python/v2/gen_html.py -i svr/knw/w2v -o svr/html_knw/w2v.html

#win_log
python python/gen_html.py -i svr/k_nearest/word/win_log_line2 -o svr/html/word/win_log_line2.html
python python/gen_html.py -i svr/k_nearest/word/win_log_line1 -o svr/html/word/win_log_line1.html
python python/gen_html.py -i svr/k_nearest/emoji/win_log_line2 -o svr/html/emoji/win_log_line2.html
python python/gen_html.py -i svr/k_nearest/emoji/win_log_line1 -o svr/html/emoji/win_log_line1.html
python python/gen_html.py -i svr/k_nearest/emoticon/win_log_line2 -o svr/html/emoticon/win_log_line2.html
python python/gen_html.py -i svr/k_nearest/emoticon/win_log_line1 -o svr/html/emoticon/win_log_line1.html
python python/gen_html.py -i svr/k_nearest/all/win_log_line2 -o svr/html/all/win_log_line2.html
python python/gen_html.py -i svr/k_nearest/all/win_log_line1 -o svr/html/all/win_log_line1.html

#win_pmi
python python/gen_html.py -i svr/k_nearest/word/win_pmi_line2 -o svr/html/word/win_pmi_line2.html
python python/gen_html.py -i svr/k_nearest/word/win_pmi_line1 -o svr/html/word/win_pmi_line1.html
python python/gen_html.py -i svr/k_nearest/emoji/win_pmi_line2 -o svr/html/emoji/win_pmi_line2.html
python python/gen_html.py -i svr/k_nearest/emoji/win_pmi_line1 -o svr/html/emoji/win_pmi_line1.html
python python/gen_html.py -i svr/k_nearest/emoticon/win_pmi_line2 -o svr/html/emoticon/win_pmi_line2.html
python python/gen_html.py -i svr/k_nearest/emoticon/win_pmi_line1 -o svr/html/emoticon/win_pmi_line1.html
python python/gen_html.py -i svr/k_nearest/all/win_pmi_line2 -o svr/html/all/win_pmi_line2.html
python python/gen_html.py -i svr/k_nearest/all/win_pmi_line1 -o svr/html/all/win_pmi_line1.html

#win_raw
python python/gen_html.py -i svr/k_nearest/word/win_raw_line2 -o svr/html/word/win_raw_line2.html
python python/gen_html.py -i svr/k_nearest/word/win_raw_line1 -o svr/html/word/win_raw_line1.html
python python/gen_html.py -i svr/k_nearest/emoji/win_raw_line2 -o svr/html/emoji/win_raw_line2.html
python python/gen_html.py -i svr/k_nearest/emoji/win_raw_line1 -o svr/html/emoji/win_raw_line1.html
python python/gen_html.py -i svr/k_nearest/emoticon/win_raw_line2 -o svr/html/emoticon/win_raw_line2.html
python python/gen_html.py -i svr/k_nearest/emoticon/win_raw_line1 -o svr/html/emoticon/win_raw_line1.html
python python/gen_html.py -i svr/k_nearest/all/win_raw_line2 -o svr/html/all/win_raw_line2.html
python python/gen_html.py -i svr/k_nearest/all/win_raw_line1 -o svr/html/all/win_raw_line1.html

#msg_log
python python/gen_html.py -i svr/k_nearest/word/msg_log_line2 -o svr/html/word/msg_log_line2.html
python python/gen_html.py -i svr/k_nearest/word/msg_log_line1 -o svr/html/word/msg_log_line1.html
python python/gen_html.py -i svr/k_nearest/emoji/msg_log_line2 -o svr/html/emoji/msg_log_line2.html
python python/gen_html.py -i svr/k_nearest/emoji/msg_log_line1 -o svr/html/emoji/msg_log_line1.html
python python/gen_html.py -i svr/k_nearest/emoticon/msg_log_line2 -o svr/html/emoticon/msg_log_line2.html
python python/gen_html.py -i svr/k_nearest/emoticon/msg_log_line1 -o svr/html/emoticon/msg_log_line1.html
python python/gen_html.py -i svr/k_nearest/all/msg_log_line2 -o svr/html/all/msg_log_line2.html
python python/gen_html.py -i svr/k_nearest/all/msg_log_line1 -o svr/html/all/msg_log_line1.html

#msg_pmi
python python/gen_html.py -i svr/k_nearest/word/msg_pmi_line2 -o svr/html/word/msg_pmi_line2.html
python python/gen_html.py -i svr/k_nearest/word/msg_pmi_line1 -o svr/html/word/msg_pmi_line1.html
python python/gen_html.py -i svr/k_nearest/emoji/msg_pmi_line2 -o svr/html/emoji/msg_pmi_line2.html
python python/gen_html.py -i svr/k_nearest/emoji/msg_pmi_line1 -o svr/html/emoji/msg_pmi_line1.html
python python/gen_html.py -i svr/k_nearest/emoticon/msg_pmi_line2 -o svr/html/emoticon/msg_pmi_line2.html
python python/gen_html.py -i svr/k_nearest/emoticon/msg_pmi_line1 -o svr/html/emoticon/msg_pmi_line1.html
python python/gen_html.py -i svr/k_nearest/all/msg_pmi_line2 -o svr/html/all/msg_pmi_line2.html
python python/gen_html.py -i svr/k_nearest/all/msg_pmi_line1 -o svr/html/all/msg_pmi_line1.html

#msg_raw
python python/gen_html.py -i svr/k_nearest/word/msg_raw_line2 -o svr/html/word/msg_raw_line2.html
python python/gen_html.py -i svr/k_nearest/word/msg_raw_line1 -o svr/html/word/msg_raw_line1.html
python python/gen_html.py -i svr/k_nearest/emoji/msg_raw_line2 -o svr/html/emoji/msg_raw_line2.html
python python/gen_html.py -i svr/k_nearest/emoji/msg_raw_line1 -o svr/html/emoji/msg_raw_line1.html
python python/gen_html.py -i svr/k_nearest/emoticon/msg_raw_line2 -o svr/html/emoticon/msg_raw_line2.html
python python/gen_html.py -i svr/k_nearest/emoticon/msg_raw_line1 -o svr/html/emoticon/msg_raw_line1.html
python python/gen_html.py -i svr/k_nearest/all/msg_raw_line2 -o svr/html/all/msg_raw_line2.html
python python/gen_html.py -i svr/k_nearest/all/msg_raw_line1 -o svr/html/all/msg_raw_line1.html

#w2v
python python/gen_html.py -i svr/k_nearest/word/w2v -o svr/html/word/w2v.html
python python/gen_html.py -i svr/k_nearest/emoji/w2v -o svr/html/emoji/w2v.html
python python/gen_html.py -i svr/k_nearest/emoticon/w2v -o svr/html/emoticon/w2v.html
python python/gen_html.py -i svr/k_nearest/all/w2v -o svr/html/all/w2v.html