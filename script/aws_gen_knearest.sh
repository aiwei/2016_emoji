#!/usr/bin/env bash

CLUSTER_ID='j-1ZQ3CEXXS3NKV'
N_EXEC=116

# win_log_line2 , -k-nearest,150, _k150
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_all_win_log_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,-k-nearest,150,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_log_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/all/win_log_line2_k150,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed]"

aws emr add-steps \
--cluster-id $CLUSTER_ID \
--steps Type=Spark,Name='kn_word_win_log_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,-k-nearest,150,
  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_log_line2,
  -out-file,s3://beidaemr/aiwei/k_nearest/word/win_log_line2_k150,
  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
  -select,word]"

#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoji_win_log_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,-k-nearest,150,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_log_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoji/win_log_line2_k150,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoji]"

#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoticon_win_log_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,-k-nearest,150,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_log_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoticon/win_log_line2_k150,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoticon]"

# win_log_line1
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_all_win_log_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,-k-nearest,150,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_log_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/all/win_log_line1_k150,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed]"

aws emr add-steps \
--cluster-id $CLUSTER_ID \
--steps Type=Spark,Name='kn_word_win_log_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,-k-nearest,150,
  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_log_line1,
  -out-file,s3://beidaemr/aiwei/k_nearest/word/win_log_line1_k150,
  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
  -select,word]"

#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoji_win_log_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,-k-nearest,150,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_log_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoji/win_log_line1_k150,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoji]"

#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoticon_win_log_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,-k-nearest,150,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_log_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoticon/win_log_line1_k150,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoticon]"

# win_raw_line2
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_all_win_raw_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_raw_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/all/win_raw_line2,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_word_win_raw_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_raw_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/word/win_raw_line2,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,word]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoji_win_raw_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_raw_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoji/win_raw_line2,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoji]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoticon_win_raw_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_raw_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoticon/win_raw_line2,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoticon]"
#
## win_raw_line1
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_all_win_raw_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_raw_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/all/win_raw_line1,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_word_win_raw_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_raw_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/word/win_raw_line1,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,word]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoji_win_raw_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_raw_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoji/win_raw_line1,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoji]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoticon_win_raw_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_raw_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoticon/win_raw_line1,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoticon]"
#
## win_pmi_line2
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_all_win_pmi_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_pmi_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/all/win_pmi_line2,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_word_win_pmi_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_pmi_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/word/win_pmi_line2,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,word]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoji_win_pmi_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_pmi_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoji/win_pmi_line2,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoji]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoticon_win_pmi_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_pmi_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoticon/win_pmi_line2,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoticon]"
#
## win_pmi_line1
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_all_win_pmi_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_pmi_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/all/win_pmi_line1,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_word_win_pmi_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_pmi_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/word/win_pmi_line1,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,word]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoji_win_pmi_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_pmi_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoji/win_pmi_line1,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoji]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoticon_win_pmi_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/win_pmi_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoticon/win_pmi_line1,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoticon]"
#
## msg_log_line2
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_all_msg_log_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_log_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/all/msg_log_line2,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_word_msg_log_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_log_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/word/msg_log_line2,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,word]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoji_msg_log_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_log_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoji/msg_log_line2,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoji]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoticon_msg_log_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_log_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoticon/msg_log_line2,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoticon]"
#
## msg_log_line1
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_all_msg_log_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_log_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/all/msg_log_line1,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_word_msg_log_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_log_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/word/msg_log_line1,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,word]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoji_msg_log_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_log_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoji/msg_log_line1,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoji]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoticon_msg_log_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_log_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoticon/msg_log_line1,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoticon]"
#
## msg_raw_line2
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_all_msg_raw_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_raw_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/all/msg_raw_line2,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_word_msg_raw_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_raw_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/word/msg_raw_line2,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,word]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoji_msg_raw_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_raw_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoji/msg_raw_line2,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoji]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoticon_msg_raw_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_raw_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoticon/msg_raw_line2,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoticon]"
#
## msg_raw_line1
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_all_msg_raw_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_raw_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/all/msg_raw_line1,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_word_msg_raw_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_raw_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/word/msg_raw_line1,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,word]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoji_msg_raw_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_raw_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoji/msg_raw_line1,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoji]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoticon_msg_raw_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_raw_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoticon/msg_raw_line1,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoticon]"
#
## msg_pmi_line2
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_all_msg_pmi_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_pmi_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/all/msg_pmi_line2,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_word_msg_pmi_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_pmi_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/word/msg_pmi_line2,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,word]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoji_msg_pmi_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_pmi_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoji/msg_pmi_line2,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoji]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoticon_msg_pmi_line2',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_pmi_line2,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoticon/msg_pmi_line2,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoticon]"
#
## msg_pmi_line1
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_all_msg_pmi_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_pmi_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/all/msg_pmi_line1,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_word_msg_pmi_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_pmi_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/word/msg_pmi_line1,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,word]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoji_msg_pmi_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_pmi_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoji/msg_pmi_line1,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoji]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoticon_msg_pmi_line1',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/msg_pmi_line1,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoticon/msg_pmi_line1,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoticon]"
#
##w2v
#N_EXEC=64
#CLUSTER_ID="j-1JUV2960D4O26"
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_all_w2v',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/w2v,
#  -out-file,s3://beidaemr/aiwei/k_nearest/all/w2v,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_word_w2v',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/w2v,
#  -out-file,s3://beidaemr/aiwei/k_nearest/word/w2v,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,word]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoji_w2v',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/w2v,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoji/w2v,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoji]"
#
#aws emr add-steps \
#--cluster-id $CLUSTER_ID \
#--steps Type=Spark,Name='kn_emoticon_w2v',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,$N_EXEC, \
#  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar,
#  -embedding,s3://beidaemr/aiwei/filtered_embedding/w2v,
#  -out-file,s3://beidaemr/aiwei/k_nearest/emoticon/w2v,
#  -word-index,s3://beidaemr/aiwei/filtered_embedding/index_typed,
#  -select,emoticon]"
