#!/bin/bash

## log
#svr/bin/line -train svr/edge/win_log \
# -output svr/embedding/win_log_line2 -order 2 -samples 40000 -threads 36
#svr/bin/line -train svr/edge/win_log \
# -output svr/embedding/win_log_line1 -order 1 -samples 40000 -threads 36

#svr/bin/line -train svr/edge/msg_log \
# -output svr/embedding/msg_log_line2 -order 2 -samples 40000 -threads 36
#svr/bin/line -train svr/edge/msg_log \
# -output svr/embedding/msg_log_line1 -order 1 -samples 40000 -threads 36

# pmi
svr/bin/line -train svr/edge/win_pmi \
 -output svr/embedding/win_pmi_line2 -order 2 -samples 40000 -threads 39
svr/bin/line -train svr/edge/win_pmi \
 -output svr/embedding/win_pmi_line1 -order 1 -samples 40000 -threads 39

svr/bin/line -train svr/edge/msg_pmi \
 -output svr/embedding/msg_pmi_line2 -order 2 -samples 40000 -threads 39
#svr/bin/line -train svr/edge/msg_pmi \
# -output svr/embedding/msg_pmi_line1 -order 1 -samples 40000 -threads 36
#
# raw
#svr/bin/line -train svr/edge/win_raw \
# -output svr/embedding/win_raw_line2 -order 2 -samples 40000 -threads 36
#svr/bin/line -train svr/edge/win_raw \
# -output svr/embedding/win_raw_line1 -order 1 -samples 40000 -threads 36
#
#svr/bin/line -train svr/edge/msg_raw \
# -output svr/embedding/msg_raw_line2 -order 2 -samples 40000 -threads 36
#svr/bin/line -train svr/edge/msg_raw \
# -output svr/embedding/msg_raw_line1 -order 1 -samples 40000 -threads 36
