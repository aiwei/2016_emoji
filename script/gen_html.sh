#!/bin/bash
#python python/v2/gen_html.py -i svr/knn/win_raw -o svr/html/win_raw.html
#python python/v2/gen_html.py -i svr/knn/win_log -o svr/html/win_log.html
#python python/v2/gen_html.py -i svr/knn/win_pmi -o svr/html/win_pmi.html
#python python/v2/gen_html.py -i svr/knn/msg_raw -o svr/html/msg_raw.html
#python python/v2/gen_html.py -i svr/knn/msg_log -o svr/html/msg_log.html
#python python/v2/gen_html.py -i svr/knn/msg_pmi -o svr/html/msg_pmi.html
#python python/v2/gen_html.py -i svr/knn/w2v -o svr/html/w2v.html

python python/v2/gen_html.py -i svr/knw/win_raw -o svr/html_knw/win_raw.html
python python/v2/gen_html.py -i svr/knw/win_log -o svr/html_knw/win_log.html
python python/v2/gen_html.py -i svr/knw/win_pmi -o svr/html_knw/win_pmi.html
python python/v2/gen_html.py -i svr/knw/msg_raw -o svr/html_knw/msg_raw.html
python python/v2/gen_html.py -i svr/knw/msg_log -o svr/html_knw/msg_log.html
python python/v2/gen_html.py -i svr/knw/msg_pmi -o svr/html_knw/msg_pmi.html
python python/v2/gen_html.py -i svr/knw/w2v -o svr/html_knw/w2v.html
