#!/usr/bin/env bash

gen_stats(){
    scala -J-Xmx32g -cp svr/bin/v2.jar GenEmojiStats \
        -dist-file svr/emoji_stats/dist/$1 \
        -freq-file svr/emoji_stats/freq/$1 \
        -embedding $1 -k 20
}

gen_stats win_log_line2
gen_stats win_log_line1
gen_stats msg_log_line2
gen_stats msg_log_line1
gen_stats win_raw_line2
gen_stats win_raw_line1
gen_stats msg_raw_line2
gen_stats msg_raw_line1
gen_stats win_pmi_line2
gen_stats win_pmi_line1
gen_stats msg_pmi_line2
gen_stats msg_pmi_line1
gen_stats w2v
