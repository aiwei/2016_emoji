#!/usr/bin/env bash
aws s3 sync s3://beidaemr/aiwei/k_nearest svr/s3/k_nearest

# win_log
cat svr/s3/k_nearest/word/win_log_line2/* > svr/k_nearest/word/win_log_line2
cat svr/s3/k_nearest/word/win_log_line1/* > svr/k_nearest/word/win_log_line1
cat svr/s3/k_nearest/emoji/win_log_line2/* > svr/k_nearest/emoji/win_log_line2
cat svr/s3/k_nearest/emoji/win_log_line1/* > svr/k_nearest/emoji/win_log_line1
cat svr/s3/k_nearest/emoticon/win_log_line2/* > svr/k_nearest/emoticon/win_log_line2
cat svr/s3/k_nearest/emoticon/win_log_line1/* > svr/k_nearest/emoticon/win_log_line1
cat svr/s3/k_nearest/all/win_log_line2/* > svr/k_nearest/all/win_log_line2
cat svr/s3/k_nearest/all/win_log_line1/* > svr/k_nearest/all/win_log_line1

# win_pmi
cat svr/s3/k_nearest/word/win_pmi_line2/* > svr/k_nearest/word/win_pmi_line2
cat svr/s3/k_nearest/word/win_pmi_line1/* > svr/k_nearest/word/win_pmi_line1
cat svr/s3/k_nearest/emoji/win_pmi_line2/* > svr/k_nearest/emoji/win_pmi_line2
cat svr/s3/k_nearest/emoji/win_pmi_line1/* > svr/k_nearest/emoji/win_pmi_line1
cat svr/s3/k_nearest/emoticon/win_pmi_line2/* > svr/k_nearest/emoticon/win_pmi_line2
cat svr/s3/k_nearest/emoticon/win_pmi_line1/* > svr/k_nearest/emoticon/win_pmi_line1
cat svr/s3/k_nearest/all/win_pmi_line2/* > svr/k_nearest/all/win_pmi_line2
cat svr/s3/k_nearest/all/win_pmi_line1/* > svr/k_nearest/all/win_pmi_line1

# win_raw
cat svr/s3/k_nearest/word/win_raw_line2/* > svr/k_nearest/word/win_raw_line2
cat svr/s3/k_nearest/word/win_raw_line1/* > svr/k_nearest/word/win_raw_line1
cat svr/s3/k_nearest/emoji/win_raw_line2/* > svr/k_nearest/emoji/win_raw_line2
cat svr/s3/k_nearest/emoji/win_raw_line1/* > svr/k_nearest/emoji/win_raw_line1
cat svr/s3/k_nearest/emoticon/win_raw_line2/* > svr/k_nearest/emoticon/win_raw_line2
cat svr/s3/k_nearest/emoticon/win_raw_line1/* > svr/k_nearest/emoticon/win_raw_line1
cat svr/s3/k_nearest/all/win_raw_line2/* > svr/k_nearest/all/win_raw_line2
cat svr/s3/k_nearest/all/win_raw_line1/* > svr/k_nearest/all/win_raw_line1

# msg_log
cat svr/s3/k_nearest/word/msg_log_line2/* > svr/k_nearest/word/msg_log_line2
cat svr/s3/k_nearest/word/msg_log_line1/* > svr/k_nearest/word/msg_log_line1
cat svr/s3/k_nearest/emoji/msg_log_line2/* > svr/k_nearest/emoji/msg_log_line2
cat svr/s3/k_nearest/emoji/msg_log_line1/* > svr/k_nearest/emoji/msg_log_line1
cat svr/s3/k_nearest/emoticon/msg_log_line2/* > svr/k_nearest/emoticon/msg_log_line2
cat svr/s3/k_nearest/emoticon/msg_log_line1/* > svr/k_nearest/emoticon/msg_log_line1
cat svr/s3/k_nearest/all/msg_log_line2/* > svr/k_nearest/all/msg_log_line2
cat svr/s3/k_nearest/all/msg_log_line1/* > svr/k_nearest/all/msg_log_line1

# msg_pmi
cat svr/s3/k_nearest/word/msg_pmi_line2/* > svr/k_nearest/word/msg_pmi_line2
cat svr/s3/k_nearest/word/msg_pmi_line1/* > svr/k_nearest/word/msg_pmi_line1
cat svr/s3/k_nearest/emoji/msg_pmi_line2/* > svr/k_nearest/emoji/msg_pmi_line2
cat svr/s3/k_nearest/emoji/msg_pmi_line1/* > svr/k_nearest/emoji/msg_pmi_line1
cat svr/s3/k_nearest/emoticon/msg_pmi_line2/* > svr/k_nearest/emoticon/msg_pmi_line2
cat svr/s3/k_nearest/emoticon/msg_pmi_line1/* > svr/k_nearest/emoticon/msg_pmi_line1
cat svr/s3/k_nearest/all/msg_pmi_line2/* > svr/k_nearest/all/msg_pmi_line2
cat svr/s3/k_nearest/all/msg_pmi_line1/* > svr/k_nearest/all/msg_pmi_line1

# msg_raw
cat svr/s3/k_nearest/word/msg_raw_line2/* > svr/k_nearest/word/msg_raw_line2
cat svr/s3/k_nearest/word/msg_raw_line1/* > svr/k_nearest/word/msg_raw_line1
cat svr/s3/k_nearest/emoji/msg_raw_line2/* > svr/k_nearest/emoji/msg_raw_line2
cat svr/s3/k_nearest/emoji/msg_raw_line1/* > svr/k_nearest/emoji/msg_raw_line1
cat svr/s3/k_nearest/emoticon/msg_raw_line2/* > svr/k_nearest/emoticon/msg_raw_line2
cat svr/s3/k_nearest/emoticon/msg_raw_line1/* > svr/k_nearest/emoticon/msg_raw_line1
cat svr/s3/k_nearest/all/msg_raw_line2/* > svr/k_nearest/all/msg_raw_line2
cat svr/s3/k_nearest/all/msg_raw_line1/* > svr/k_nearest/all/msg_raw_line1

# w2v
cat svr/s3/k_nearest/word/w2v/* > svr/k_nearest/word/w2v
cat svr/s3/k_nearest/emoji/w2v/* > svr/k_nearest/emoji/w2v
cat svr/s3/k_nearest/emoticon/w2v/* > svr/k_nearest/emoticon/w2v
cat svr/s3/k_nearest/all/w2v/* > svr/k_nearest/all/w2v
