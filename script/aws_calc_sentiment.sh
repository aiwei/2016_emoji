#!/usr/bin/env bash
aws emr add-steps \
--cluster-id j-1RTKEAB9GFT91 \
--steps Type=Spark,Name='calc_sentiment',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,117, \
  --class,CalcSentiment,s3://beidaemr/aiwei/jars/v2.jar]"