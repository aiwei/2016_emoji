#!/usr/bin/env bash
aws emr add-steps \
--cluster-id j-3QIYYN58B1RW1 \
--steps Type=Spark,Name='calc_shift',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,45g,--driver-cores,6,--executor-memory,45g,--executor-cores,6,--num-executors,24, \
  --conf,spark.driver.maxResultSize=0, \
  --class,CalcVecShift,s3://beidaemr/aiwei/jars/v2.jar]"
