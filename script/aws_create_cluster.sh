#!/usr/bin/env bash
aws emr create-cluster \
--release-label emr-5.0.0 \
--instance-groups \
  'InstanceGroupType=MASTER,InstanceCount=1,InstanceType=m4.xlarge' \
  'InstanceGroupType=CORE,InstanceCount=9,InstanceType=m4.10xlarge,EbsConfiguration={EbsOptimized=true,EbsBlockDeviceConfigs=[{VolumeSpecification={SizeInGB=200,VolumeType=gp2},VolumesPerInstance=1}]}' \
--no-auto-terminate \
--service-role EMR_DefaultRole \
--name gaibang \
--log-uri 's3n://aws-logs-076480233801-us-east-1/elasticmapreduce/' \
--ec2-attributes KeyName=heimuya-aiwei,InstanceProfile=EMR_EC2_DefaultRole,SubnetId=subnet-b4c845ed \
--enable-debugging \
--applications Name=Hadoop Name=Spark
