#!/usr/bin/env bash
aws emr add-steps \
--cluster-id j-39IY4H177A252 \
--steps Type=Spark,Name='gen_sentence',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,4, \
  --class,GenSentences,s3://beidaemr/aiwei/jars/v2.jar]"