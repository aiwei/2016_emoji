#!/usr/bin/env bash

aws s3 sync s3://beidaemr/aiwei/index svr/s3/index
cat svr/s3/index/win/* > svr/index/win
cat svr/s3/index/msg/* > svr/index/msg
rm -r svr/s3/index

aws s3 sync s3://beidaemr/aiwei/count svr/s3/count
cat svr/s3/count/win/* > svr/count/win
cat svr/s3/count/msg/* > svr/count/msg
rm -r svr/s3/count

aws s3 sync s3://beidaemr/aiwei/edge svr/s3/edge
cat svr/s3/edge/win/* > svr/edge/win_raw
cat svr/s3/edge/msg/* > svr/edge/msg_raw
rm -r svr/s3/edge

aws s3 sync s3://beidaemr/aiwei/sentences
cat svr/s3/sentences/* > svr/sentences
rm -r svr/s3/sentences