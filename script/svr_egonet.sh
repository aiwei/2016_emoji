#!/usr/bin/env bash

k=20
scala -J-Xmx16g -cp svr/bin/v2.jar emojistats.EgoNet -nn-file svr/k_nearest/all/win_log_line2 -out-file svr/emoji_stats/ego$k/win_log_line2 -k $k
scala -J-Xmx16g -cp svr/bin/v2.jar emojistats.EgoNet -nn-file svr/k_nearest/all/win_log_line1 -out-file svr/emoji_stats/ego$k/win_log_line1 -k $k
scala -J-Xmx16g -cp svr/bin/v2.jar emojistats.EgoNet -nn-file svr/k_nearest/all/win_pmi_line2 -out-file svr/emoji_stats/ego$k/win_pmi_line2 -k $k
scala -J-Xmx16g -cp svr/bin/v2.jar emojistats.EgoNet -nn-file svr/k_nearest/all/win_pmi_line1 -out-file svr/emoji_stats/ego$k/win_pmi_line1 -k $k
scala -J-Xmx16g -cp svr/bin/v2.jar emojistats.EgoNet -nn-file svr/k_nearest/all/win_raw_line2 -out-file svr/emoji_stats/ego$k/win_raw_line2 -k $k
scala -J-Xmx16g -cp svr/bin/v2.jar emojistats.EgoNet -nn-file svr/k_nearest/all/win_raw_line1 -out-file svr/emoji_stats/ego$k/win_raw_line1 -k $k
scala -J-Xmx16g -cp svr/bin/v2.jar emojistats.EgoNet -nn-file svr/k_nearest/all/msg_log_line2 -out-file svr/emoji_stats/ego$k/msg_log_line2 -k $k
scala -J-Xmx16g -cp svr/bin/v2.jar emojistats.EgoNet -nn-file svr/k_nearest/all/msg_log_line1 -out-file svr/emoji_stats/ego$k/msg_log_line1 -k $k
scala -J-Xmx16g -cp svr/bin/v2.jar emojistats.EgoNet -nn-file svr/k_nearest/all/msg_pmi_line2 -out-file svr/emoji_stats/ego$k/msg_pmi_line2 -k $k
scala -J-Xmx16g -cp svr/bin/v2.jar emojistats.EgoNet -nn-file svr/k_nearest/all/msg_pmi_line1 -out-file svr/emoji_stats/ego$k/msg_pmi_line1 -k $k
scala -J-Xmx16g -cp svr/bin/v2.jar emojistats.EgoNet -nn-file svr/k_nearest/all/msg_raw_line2 -out-file svr/emoji_stats/ego$k/msg_raw_line2 -k $k
scala -J-Xmx16g -cp svr/bin/v2.jar emojistats.EgoNet -nn-file svr/k_nearest/all/msg_raw_line1 -out-file svr/emoji_stats/ego$k/msg_raw_line1 -k $k
scala -J-Xmx16g -cp svr/bin/v2.jar emojistats.EgoNet -nn-file svr/k_nearest/all/w2v -out-file svr/emoji_stats/ego$k/w2v -k $k
