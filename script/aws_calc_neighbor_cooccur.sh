#!/usr/bin/env bash
aws emr add-steps \
--cluster-id j-7A1PLIZ1JLI5 \
--steps Type=Spark,Name='calc_dist',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,45g,--driver-cores,10,--executor-memory,75g,--executor-cores,6,--num-executors,24, \
  --conf,spark.driver.maxResultSize=0, \
  --class,CalcNeighborCooccur,s3://beidaemr/aiwei/jars/v2.jar]"
