#!/usr/bin/env bash
aws emr add-steps \
--cluster-id j-181J8MUCWAF6J \
--steps Type=Spark,Name='kn-emoticon-win_log',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,117, \
  --class,GenNN,s3://beidaemr/aiwei/jars/v2.jar, \
  -embedding,s3://beidaemr/aiwei/v2/filtered/win_log, \
  -out-file,s3://beidaemr/aiwei/v2/k_nearest/emoticon/win_log_line2, \
  -word-index,s3://beidaemr/aiwei/v2/filtered/index_typed, \
  -select,emoticon]"
