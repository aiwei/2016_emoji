#!/usr/bin/env bash

aws emr add-steps \
--cluster-id j-181J8MUCWAF6J \
--steps Type=Spark,Name='gen_cooccur_win',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,116, \
  --class,GenCooccur,s3://beidaemr/aiwei/jars/v2.jar,
  -index-file,s3://beidaemr/aiwei/index/win,
  -count-file,s3://beidaemr/aiwei/count/win,
  -cooccur-file,s3://beidaemr/aiwei/edge_raw/win,
  -cooccur-indexed-file,s3://beidaemr/aiwei/edge/win,
  -window,5,-threshold,5,-gen-cooccur,-gen-index]"

aws emr add-steps \
--cluster-id j-181J8MUCWAF6J \
--steps Type=Spark,Name='gen_cooccur_msg',ActionOnFailure=CONTINUE,Args="[--deploy-mode,cluster,--driver-memory,10G,--driver-cores,3,--executor-memory,10G,--executor-cores,3,--num-executors,116, \
  --class,GenCooccur,s3://beidaemr/aiwei/jars/v2.jar,
  -index-file,s3://beidaemr/aiwei/index/msg,
  -count-file,s3://beidaemr/aiwei/count/msg,
  -cooccur-file,s3://beidaemr/aiwei/edge_raw/msg,
  -cooccur-indexed-file,s3://beidaemr/aiwei/edge/msg,
  -window,0,-threshold,5,-gen-cooccur,-gen-index]"
