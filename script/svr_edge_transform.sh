#!/bin/bash

# log transform
scala -J-Xmx16g src/CooccurrenceLogTransformer.scala \
 -in-file svr/edge/msg_raw \
 -out-file svr/edge/msg_log
scala -J-Xmx16g src/CooccurrenceLogTransformer.scala \
 -in-file svr/edge/win_raw \
 -out-file svr/edge/win_log

# PMI transform
scala -J-Xmx16g src/CooccurrencePmiTransformer.scala \
 -in-file svr/edge/msg_raw \
 -count-file svr/count/msg \
 -index-file svr/index/msg \
 -out-file svr/edge/msg_pmi
scala -J-Xmx16g src/CooccurrencePmiTransformer.scala \
 -in-file svr/edge/win_raw \
 -count-file svr/count/win \
 -index-file svr/index/win \
 -out-file svr/edge/win_pmi