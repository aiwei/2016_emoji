import pandas as pd
import numpy as np
from sklearn.mixture import GaussianMixture as GM

indexFile = open("svr/index/msg")
emojiIds = []
for line in indexFile:
    emojiId = int(line.split("\t")[1])
    freq = int(line.split("\t")[2])
    if emojiId > 0 and emojiId <= 1280:
        if freq < 5:
            continue
        emojiIds.append(emojiId)
    elif emojiId > 0:
        break

fout = open("svr/emoji_stats/supp/win_log_line2", "w")
fout.write("emoji_id\tn_dist\tmean0\tmean1\tmean2\tvar0\tvar1\tvar2\tw0\tw1\tw2\n")
for emojiId in emojiIds:
    print "processing emojiID = %d " % emojiId,
    df = pd.read_csv("svr/vec_shift/win_log_line2/%d" % emojiId, sep=" ", header=None).drop(0, axis=1)
    df.columns = ['length', 'cnt', 'vec_shift']
    x = df.vec_shift
    gm = GM(n_components=3, means_init=[[-4], [0], [4]]).fit(np.asmatrix(np.log(x)).T)
    fout.write("%d\t%d\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n"
               % (emojiId, x.size,
                  gm.means_[0], gm.means_[1], gm.means_[2],
                  gm.covariances_[0][0][0], gm.covariances_[1][0][0], gm.covariances_[2][0][0],
                  gm.weights_[0], gm.weights_[1], gm.weights_[2]))
    print gm.weights_[2]

fout.close()
