import argparse
import random
from collections import defaultdict

'''
# run with command:
python python/evaluation/pooling.py \
 -input_files \
  output/line2/nn_raw_filter2000 output/line2/nn_log_filter2000 output/line2/nn_log_filter2000 \
  output/line2/nn_raw_filter5000 output/line2/nn_raw_filter5000 output/line2/nn_raw_filter5000 \
  output/line3/nn_raw_filter5000 output/line3/nn_raw_filter5000 output/line3/nn_raw_filter5000 \
  output/line3/nn_raw_filter10000 output/line3/nn_log_filter10000 output/line3/nn_log_filter10000 \
  output/word2vec/nn_filter2000 output/word2vec/nn_filter5000 \
 -index_files \
  data/line2/wordindex data/line2/wordindex data/line2/wordindex \
  data/line2/wordindex data/line2/wordindex data/line2/wordindex \
  data/line3/wordindex data/line3/wordindex data/line3/wordindex \
  data/line3/wordindex data/line3/wordindex data/line3/wordindex \
  data/line2/wordindex data/line2/wordindex \
 -tags \
  msg_raw_filter2000 msg_log_filter2000 msg_pmi_filter2000 \
  msg_raw_filter5000 msg_log_filter5000 msg_pmi_filter5000 \
  win_raw_filter5000 win_log_filter5000 win_pmi_filter5000 \
  win_raw_filter10000 win_log_filter10000 win_pmi_filter10000 \
  w2v_filter2000 w2v_filter5000 \
 -output_dir output/trec_eval
'''

argParser = argparse.ArgumentParser()
argParser.add_argument("-input_files", nargs='+', type=str)
argParser.add_argument("-index_files", nargs='+', type=str)
argParser.add_argument("-tags", nargs='+', type=str)
argParser.add_argument("-output_dir", type=str)

args = argParser.parse_args()

inFiles = args.input_files
indexFiles = args.index_files
tags = args.tags
outputDir = args.output_dir
if outputDir[-1] != '/':
    outputDir += '/'

print inFiles
print indexFiles
print tags

if len(inFiles) != len(tags):
    print "please check input files and tags"
    exit()
if len(indexFiles) != len(tags):
    print "please check index files and tags"
    exit()

indexMaps = defaultdict(dict) # <tag, indexMap>
pooledMap = defaultdict(set) # <emoji_id, woreSet>
for i in range(len(tags)):
    tag = tags[i]
    print "loading " + tag
    indexFileName = indexFiles[i]

    # reuse if this index has been loaded
    for j in reversed(range(i)):
        if indexFiles[i] == indexFiles[j]:
            indexMaps[tag] = indexMaps[tags[j]]
            break
    indexMap = indexMaps[tag]

    # load index if the index has not been loaded
    if len(indexMap) == 0:
        fin = open(indexFileName)
        for line in fin:
            (word, index) = line.rstrip().split("\t")
            indexMap[index] = word
        fin.close()

    fin = open(inFiles[i])
    fout = open(outputDir + tag + "_10nn", "w")
    for line in fin:
        line = line.strip().split("\t")
        emoji_id = line[0]
        fout.write(emoji_id)
        for tup in line[1:11]:
            word = indexMap[tup.split(' ')[0]]
            if len(word) > 20:
                word = word[:18] + "..."
            fout.write("\t%s" % word)
            pooledMap[emoji_id].add(word)
        fout.write("\n")
    fin.close()
    fout.close()

fout = open(outputDir + "trec_eval_task.txt", "w")
for i in range(1282):
    emoji_id = str(i)
    if emoji_id not in pooledMap:
        continue
    words = list(pooledMap[emoji_id])
    random.shuffle(words)
    fout.write(emoji_id + "\t" + "\t".join(words) + "\n")
fout.close()
