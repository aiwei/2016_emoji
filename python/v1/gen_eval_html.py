import argparse
import random

argParser = argparse.ArgumentParser()
argParser.add_argument("-input_files", nargs='+', type=str)
argParser.add_argument("-index_files", nargs='+', type=str)
argParser.add_argument("-count_files", nargs='+', type=str)
argParser.add_argument("-tags", nargs='+', type=str)
argParser.add_argument("-samples", type=int, default=100)
argParser.add_argument("-files", type=int, default=10)

args = argParser.parse_args()

inFiles = args.input_files
indexFiles = args.index_files
countFiles = args.count_files
tags = args.tags
nSample = args.samples
nFile = args.files

print inFiles
print tags

if len(inFiles) != len(tags):
    print "please check input files and tags"
    exit()
if len(indexFiles) != len(tags):
    print "please check index files and tags"
    exit()
if len(countFiles) != len(tags):
    print "please check count files and tags"
    exit()

class Row:
    def __init__(self, _emoji_id, _col):
        self.emoji_id = _emoji_id
        self.col = _col
        self.cells = []
    def addCell(self, tag, word, count):
        self.cells += [(tag, word, count)]

dataMap = {}
for i in range(len(tags)):
    tag = tags[i]
    print "loading " + tag

    fin = open(indexFiles[i])
    indexMap = {}
    for line in fin:
        (word, index) = line.rstrip().split()
        indexMap[index] = word
    fin.close()

    fin = open(countFiles[i])
    countMap = {}
    for line in fin:
        (word, count) = line.rstrip().split()
        countMap[word] = count
    fin.close()

    tagMap = {}

    fin = open(inFiles[i])
    for line in fin:
        line = line.strip().split("\t")
        emoji_id = line[0]
        words = []
        for tup in line[1:]:
            word = tup.split(' ')[0]
            words.append((indexMap[word], countMap[indexMap[word]]))
        tagMap[emoji_id] = words

    dataMap[tag] = tagMap

used = set()
for iFile in range(nFile):
    fhtml = open("output/eval/%d.html" % iFile, "w")

    fhtml.write('''\
<html>
<head>
<style>table td{width:40px;height:24px;padding-left:5px;padding-right:5px;padding-top:0px;padding-bottom:0px;text-align:center;}table td.highlighted {background-color:#0e0;}.tag{display:none}.cnt{display:none}</style>
<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script>
$(function(){var isMouseDown=false,isHighlighted;$("#our_table td").mousedown(function(){isMouseDown=true;$(this).toggleClass("highlighted");isHighlighted = $(this).hasClass("highlighted");return false;}).mouseover(function(){if(isMouseDown){$(this).toggleClass("highlighted",isHighlighted);}});$(document).mouseup(function(){isMouseDown = false;});});
var textFile = null;fnSend = function(){txt = "";$("tr").each(function(){$(this).find("td").each(function(){var tag=$(this).find(".tag").text();var cnt=$(this).find(".cnt").text();var isClick="0 ";if ($(this).hasClass("highlighted")){isClick = "1 "};txt+=tag+" "+cnt+" "+isClick+"\\n"})});
var data = new Blob([txt], {type: "text/plain"});if(textFile !== null){window.URL.revokeObjectURL(textFile);}textFile = window.URL.createObjectURL(data);$("#dLink").css("display", "block");$("#dLink").attr("href", textFile)}
</script></head>
<body>
  <table cellpadding="0" cellspacing="0" id="our_table" border="1">\n\
    ''')
    reorderedTags = list(tags)
    validEmojis = set([str(x) for x in range(1281)])
    for (tag, tagMap) in dataMap.iteritems():
        validEmojis.intersection_update(set(tagMap.keys()))
    validEmojis = list(validEmojis)
    for iSample in range(nSample):
        emojiId = random.choice(validEmojis)
        col = random.randint(0, 4)
        while emojiId + "," + str(col) in used:
            emojiId = random.choice(validEmojis)
            col = random.randint(0, 4)
        used.add(emojiId + "," + str(col))
        emojiId = str(emojiId)
        fhtml.write('<tr><td class=word><img src=../../resource/icons/%s.png></td>' % emojiId)
        random.shuffle(reorderedTags)
        for tag in reorderedTags:
            (word, count) = dataMap[tag][emojiId][col]
            if len(word) > 20:
                word = word[:18] + "..."
            fhtml.write("<td><a class=tag>%s</a><a class=cnt>%s</a>%s</td>" % (tag, count, word))
        fhtml.write('</tr>\n')
    fhtml.write('''\
</table>
<button type="button" onclick="fnSend()">Submit</button>
<a download="%d.txt" id="dLink" style="display: none">Download</a>
</body>\n\
    ''' % iFile)
