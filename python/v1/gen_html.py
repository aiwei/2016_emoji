#!/Users/aiwei/anaconda/bin/python
import argparse

argParser = argparse.ArgumentParser()
argParser.add_argument("-filter", type=int, default=1000,
                       help="filter word occurring less than a threshold")
argParser.add_argument("-n", type=int, default=20,
                       help="number of rows")
argParser.add_argument("-i", "--input", type=str, help="input file")
argParser.add_argument("-o", "--output", type=str, help="output file")
argParser.add_argument("--index", type=str, help="index file")
argParser.add_argument("--count", type=str, help="count file")

args = argParser.parse_args()

n_keep = args.n
n_filter = args.filter
infile = args.input
outfile = args.output
indexFile = args.index
countFile = args.count

# Load index
indexMap = {}
fin = open(indexFile)
for line in fin:
    (word, index) = line.rstrip().split("\t")
    indexMap[index] = word
fin.close()

# Load Index Count
countMap = {}
fin = open(countFile)
for line in fin:
    (word, count) = line.rstrip().split("\t")
    countMap[word] = count
fin.close()

# Load emoji icons
# emojiIcons = {}
# fin = open("resource/emoji-icon.tsv", "r")
# for line in fin:
#     (emoji, icon) = line.rstrip().split("\t")
#     emojiIcons[emoji] = icon
# fin.close()

fout = open(outfile, "w")

# print html header
fout.write('''\
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<style>
td {
    height: 24px;
    width: 40px;
    padding-left: 5px;
    padding-right: 5px;
    padding-top: 0px;
    padding-bottom: 0px;
}
img {
    width: 20px;
    height: 20px;
}
</style>
</head>
<body>
<table cellspacing="0" cellpadding="0" border="0">
<tr>
  <td id="firstTd" style="padding: 0px"></td>
  <td style="padding: 0px"><div class = "col2" id="divHeader" style="overflow:hidden;width:800px">
  <table cellspacing="0" cellpadding="0" border="1" ><tr>
''')

# print table header, customizable
for col in range(n_keep):
    fout.write('    <td><div class="tableHeader">%d</div></td>\n' % col)
    fout.write('    <td><div class="tableHeader">%s</div></td>\n' % "freq")
    fout.write('    <td><div class="tableHeader">%s</div></td>\n' % "dist")
fout.write('    <td><div class="tableHeader">%s</div></td>\n' % "NULL")

# print end of table header, start of freezing column
fout.write('''\
  </tr></table>
  </div></td>
</tr>
<tr>
  <td valign="top" style="padding: 0px"><div id="firstcol" style="overflow: hidden;height:100px">
  <table width="150px" cellspacing="0" cellpadding="0" border="1" >
''')

# print the freezing column(s), customizable
# for row in range(40):
#    fout.write('    <tr><td class="tableFirstCol">%s</td><td class="tableFirstCol">abc</td></tr>\n' % str(row))
# fout.write('    <tr><td class="tableFirstCol">%s</td><td class="tableFirstCol">abc</td></tr>\n' % 'XX')
fin = open(infile, "r")
formatString = '      <td>%s</td>\n'
for line in fin:
    emojiIndex = line.rstrip().split("\t")[0]
    if indexMap[emojiIndex] not in countMap:
        continue
    emojiId = str(int(emojiIndex) - 1)
    fout.write("    <tr class='tableFirstCol'>")
    fout.write(formatString % emojiId)
    fout.write(formatString % ("<img src=resource/icons/%s.png>" % emojiId))
    fout.write(formatString % countMap[indexMap[emojiIndex]])
    fout.write("    </tr>\n")

fout.write("    <tr>")
fout.write(formatString % "NULL")
fout.write(formatString % "NULL")
fout.write(formatString % "NULL")
fout.write("    </tr>")
fin.close()

# print end of the freezing column(s), start of table content
fout.write('''\
  </table></div>
  </td>
  <td valign="top" style="padding: 0px">
  <div class = "col2" id="table_div" style="overflow: scroll;height:100px;width:800px;position:relative" onscroll="fnScroll()" >
    <table cellspacing="0" cellpadding="0" border="1" >
''')

# print table content, customizable
# for row in range(40):
#    fout.write('    <tr>\n')
#    for col in range(50):
#        fout.write('      <td>%s</td>\n' % str(row * col))
#    fout.write('    </tr>\n')
fin = open(infile, "r")
formatString = '      <td>%s</td>\n'
for line in fin:
    line = line.rstrip().split("\t")
    emojiId = line[0]
    if indexMap[emojiId] not in countMap:
        continue
    nns = line[1:]
    fout.write('    <tr>\n')
    n_left = n_keep
    for nn in nns:
        if n_left == 0:
            break
        nn = nn.split(' ')
        wordId = nn[0]
        word = indexMap[wordId]
        wordCount = countMap[word]
        if wordCount < n_filter:
            continue
        if len(word) > 20:
            word = word[:18] + "..."
        fout.write(formatString % word)
        fout.write(formatString % wordCount)
        fout.write(formatString % ("%.1f" % float(nn[1])))
        n_left -= 1
    fout.write('      <td class="adjH"></td>')
    fout.write('    </tr>\n')
fout.write("<tr>")
for i in range(n_keep):
    fout.write('<td class="adjW"></td><td class="adjW"></td><td class="adjW"></td>')
fout.write("</tr>\n")

# print footer
fout.write('''\
    </table>
  </div>
  </td>
</tr>
</table>
<script>
$(window).resize(function() {adjustSize();});
$(document).ready(function(){fnAdjustTable();adjustSize();});

adjustSize = function(){
  $('.col2').each(function(i){$(this).css("width", $(window).width() - $('#firstcol').width() - 20 - 8*2);});
  $('#firstcol').css("height", $(window).height() - $('#firstTd').height()-20-8*2)
  $('#table_div').css("height", $(window).height() - $('#firstTd').height()-20-8*2)
}

fnAdjustTable = function(){
  var colCount = $('#divHeader>table>tbody>tr>td').length;
  var m = 0; var n = 0; brow = 'mozilla';
  jQuery.each(jQuery.browser, function(i, val) {if(val == true){brow = i.toString();}});
  $('.tableHeader').each(function(i){
    if (m < colCount){
      if (brow == 'mozilla'){$(this).css('width',$('.adjW::eq('+m+')').innerWidth());}
      else if (brow == 'msie'){$('#firstTd').css("width",$('.tableFirstCol').width());$(this).css('width',$('#table_div td:eq('+m+')').width()-2);}
      else if (brow == 'safari'){$(this).css('width',$('.adjW:eq('+m+')').width());}
      else {$('#firstTd').css("width",$('.tableFirstCol').width());$(this).css('width',$('#table_div td:eq('+m+')').innerWidth());}
    }m++;});
  $('.tableFirstCol').each(function(i){
    if(brow == 'mozilla'){$(this).css('height',$('.adjH:eq('+colCount*n+')').outerHeight()+2);}
    else if(brow == 'msie'){$(this).css('height',$('#table_div td:eq('+colCount*n+')').innerHeight()-2);}
    else {$(this).css('height',$('.adjH:eq('+n+')').height()+2);}
    n++;});};

fnScroll = function(){
  $('#divHeader').scrollLeft($('#table_div').scrollLeft());
  $('#firstcol').scrollTop($('#table_div').scrollTop());
};
</script>
</body>
</html>
''')
