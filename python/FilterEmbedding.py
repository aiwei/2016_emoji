msgIndexMap = {}
fout = open("svr/filtered/index", "w")
fin = open("svr/index/msg")
for line in fin:
    word, index, cnt = line.rstrip().split("\t")
    if int(cnt) > 2000:
        msgIndexMap[word] = index
        fout.write(index + "\t" + word + "\n")
fout.close()

validMsgIndexSet = set(msgIndexMap.values())

print "msg_raw"
fout = open("svr/filtered/msg_raw", "w")
fin = open("svr/embedding/msg_raw")
for line in fin:
    msgIndex = line.split(" ")[0]
    if msgIndex in validMsgIndexSet:
        fout.write(line)
fout.close()

print "msg_log"
fout = open("svr/filtered/msg_log", "w")
fin = open("svr/embedding/msg_log")
for line in fin:
    msgIndex = line.split(" ")[0]
    if msgIndex in validMsgIndexSet:
        fout.write(line)
fout.close()

print "msg_pmi"
fout = open("svr/filtered/msg_pmi", "w")
fin = open("svr/embedding/msg_pmi")
for line in fin:
    msgIndex = line.split(" ")[0]
    if msgIndex in validMsgIndexSet:
        fout.write(line)
fout.close()

print "prepare for win"
indexTranslateMap = {}
fin = open("svr/index/win")
for line in fin:
    word, winIndex, cnt = line.split("\t")
    if word in msgIndexMap:
        indexTranslateMap[winIndex] = msgIndexMap[word]

print "win_raw"
fout = open("svr/filtered/win_raw", "w")
fin = open("svr/embedding/win_raw")
for line in fin:
    tup = line.split(" ")
    winIndex = tup[0]
    if winIndex in indexTranslateMap:
        fout.write(" ".join([indexTranslateMap[winIndex]] + tup[1:]))
fout.close()

print "win_log"
fout = open("svr/filtered/win_log", "w")
fin = open("svr/embedding/win_log")
for line in fin:
    tup = line.split(" ")
    winIndex = tup[0]
    if winIndex in indexTranslateMap:
        fout.write(" ".join([indexTranslateMap[winIndex]] + tup[1:]))
fout.close()

print "win_pmi"
fout = open("svr/filtered/win_pmi", "w")
fin = open("svr/embedding/win_pmi")
for line in fin:
    tup = line.split(" ")
    winIndex = tup[0]
    if winIndex in indexTranslateMap:
        fout.write(" ".join([indexTranslateMap[winIndex]] + tup[1:]))
fout.close()

print "w2v"
fout = open("svr/filtered/w2v", "w")
fin = open("svr/embedding/w2v")
for line in fin:
    tup = line.split(" ")
    if tup[0] in msgIndexMap:
        fout.write(" ".join([msgIndexMap[tup[0]]] + tup[1:]))
fout.close()