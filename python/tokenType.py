# -*- coding: utf-8 -*-
import re

Contractions = re.compile("(?i)(\\w+)(n['’′]t|['’′]ve|['’′]ll|['’′]d|['’′]re|['’′]s|['’′]m)$")
Whitespace = re.compile("[\\s\\p{Zs}]+")

punctChars = "['\"“”‘’.?!…,:;]"
punctSeq   = "['\"“”‘’]+|[.?!,…]+|[:;]+"
entity     = "&(?:amp|lt|gt|quot);"

urlStart1  = "(?:https?://|\\bwww\\.)"
commonTLDs = "(?:com|org|edu|gov|net|mil|aero|asia|biz|cat|coop|info|int|jobs|mobi|museum|name|pro|tel|travel|xxx)"
ccTLDs	 = "(?:ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|" + \
    "bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|" + \
    "er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|" + \
    "hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|" + \
    "lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|" + \
    "nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|" + \
    "sl|sm|sn|so|sr|ss|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|" + \
    "va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|za|zm|zw)"
urlStart2  = "\\b(?:[A-Za-z\\d-])+(?:\\.[A-Za-z0-9]+){0,3}\\." + "(?:"+commonTLDs+"|"+ccTLDs+")"+"(?:\\."+ccTLDs+")?(?=\\W|$)"
urlBody    = "(?:[^\\.\\s<>][^\\s<>]*?)?"
urlExtraCrapBeforeEnd = "(?:"+punctChars+"|"+entity+")+?"
urlEnd     = "(?:\\.\\.+|[<>]|\\s|$)"
url        = "(?:"+urlStart1+"|"+urlStart2+")"+urlBody+"(?=(?:"+urlExtraCrapBeforeEnd+")?"+urlEnd+")"


timeLike   = "\\d+(?::\\d+){1,2}"
numberWithCommas = "(?:(?<!\\d)\\d{1,3},)+?\\d{3}" + "(?=(?:[^,\\d]|$))"
numComb	 = "\\p{Sc}?\\d+(?:\\.\\d+)+%?"

boundaryNotDot = "(?:$|\\s|[“\\u0022?!,:;]|" + entity + ")"
aa1  = "(?:[A-Za-z]\\.){2,}(?=" + boundaryNotDot + ")"
aa2  = "[^A-Za-z](?:[A-Za-z]\\.){1,}[A-Za-z](?=" + boundaryNotDot + ")"
standardAbbreviations = "\\b(?:[Mm]r|[Mm]rs|[Mm]s|[Dd]r|[Ss]r|[Jj]r|[Rr]ep|[Ss]en|[Ss]t)\\."
arbitraryAbbrev = "(?:" + aa1 +"|"+ aa2 + "|" + standardAbbreviations + ")"
separators  = "(?:--+|―|—|~|–|=)"
decorations = "(?:[♫♪]+|[★☆]+|[♡]+|[\\ue001-\\uebbb]+)"
thingsThatSplitWords = "[^\\s\\.,?\"]"
embeddedApostrophe = thingsThatSplitWords+"+['’′]" + thingsThatSplitWords + "*"

def OR(parts):
    prefix="(?:"
    sb = ""
    for s in parts:
        sb += prefix
        prefix="|"
        sb += s
    sb += ")"
    return sb

normalEyes = "(?iu)[:=]"
wink = "[;]"
noseArea = "(?:|-|[^a-zA-Z0-9 ])"
happyMouths = "[D\\)\\]\\}]+"
sadMouths = "[\\(\\[\\{]+"
tongue = "[pPd3]+"
otherMouths = "(?:[oO]+|[/\\\\]+|[vV]+|[Ss]+|[|]+)"

bfLeft = "(♥|0|o|°|v|\\$|t|x|;|\\u0CA0|@|ʘ|•|・|◕|\\^|¬|\\*)"
bfCenter= "(?:[\\.]|[_-]+)"
bfRight = "\\2"
s3 = "(?:--['\"])"
s4 = "(?:<|&lt;|>|&gt;)[\\._-]+(?:<|&lt;|>|&gt;)"
s5 = "(?:[.][_]+[.])"
basicface = "(?:(?i)" +bfLeft+bfCenter+bfRight+ ")|" +s3+ "|" +s4+ "|" + s5

eeLeft = "[＼\\\\ƪԄ\\(（<>;ヽ\\-=~\\*]+"
eeRight= "[\\-=\\);'\\u0022<>ʃ）/／ノﾉ丿╯σっµ~\\*]+"
eeSymbol = "[^A-Za-z0-9\\s\\(\\)\\*:=-]"
eastEmote = eeLeft + "(?:"+basicface+"|" +eeSymbol+")+" + eeRight


emoticon = OR([
    "(?:>|&gt;)?" + OR([normalEyes, wink]) + OR([noseArea,"[Oo]"]) +
    OR([tongue+"(?=\\W|$|RT|rt|Rt)", otherMouths+"(?=\\W|$|RT|rt|Rt)", sadMouths, happyMouths]),
    "(?<=(?: |^))" + OR([sadMouths,happyMouths,otherMouths]) + noseArea + OR([normalEyes, wink]) + "(?:<|&lt;)?",
    eastEmote.replace("2", "1", 1), basicface
])

Hearts = "(?:<+/?3+)+"

Arrows = "(?:<*[-―—=]*>+|<+[-―—=]*>*)|\\p{InArrows}+"
Hashtag = "#[a-zA-Z0-9_]+"
AtMention = "[@＠][a-zA-Z0-9_]+"

Bound = "(?:\\W|^|$)"
Email = "(?<=" +Bound+ ")[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}(?=" +Bound+")"



# static v1.EmojiConst$ emojiConst = v1.EmojiConst$.MODULE$;
# static String emojis = "(?:" + emojiConst.emojiRegex() + ")";
#
Protected  = re.compile(OR([Hearts, url, Email, timeLike, numberWithCommas, numComb, emoticon, Arrows, entity, punctSeq,
    arbitraryAbbrev, separators, decorations, embeddedApostrophe, Hashtag, AtMention]))

emoticonPattern = re.compile(OR([emoticon, Hearts, Arrows, punctSeq, decorations]))

fout = open("svr/filtered/index_typed", "w")
fin = open("svr/index/msg")
wordPattern = re.compile("^[a-zA-Z0-9@#-_]+$")
for line in fin:
    word, index, cnt = line.rstrip().split("\t")
    if int(cnt) < 2000:
        continue
    if index < 1281:
        fout.write(index + "\t" + "emoji")
        continue
    if emoticonPattern.match(word):
        fout.write(index + "\t" + "emoticon")
        continue
    if wordPattern.match(word):
        fout.write(index + "\t" + "word")
fout.close()
