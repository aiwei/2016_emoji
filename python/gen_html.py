#!/Users/aiwei/anaconda/bin/python
import argparse

argParser = argparse.ArgumentParser()

argParser.add_argument("-n", type=int, default=20,
                       help="number of rows")
argParser.add_argument("-i", "--input", type=str,
                       help="input file")
argParser.add_argument("-o", "--output", type=str,
                       help="output file")
argParser.add_argument("--index", type=str, default="svr/filtered_embedding/index_typed",
                       help="index file")

args = argParser.parse_args()

n_keep = args.n
infile = args.input
outfile = args.output
indexFile = args.index

fin = open(infile)
nnMap = {}
distMap = {}
for line in fin:
    (word1, word2, dist) = line.rstrip().split("\t")
    dist = float(dist)
    if (word1 in nnMap) and (len(nnMap[word1]) >= n_keep):
        continue
    if word1 in nnMap:
        nnMap[word1].append(word2)
        distMap[word1] += dist
        if len(nnMap[word1]) == n_keep:
            distMap[word1] /= n_keep
    else:
        nnMap[word1] = [word2]
        distMap[word1] = dist

# Load index
indexMap = {}
fin = open(indexFile)
for line in fin:
    (index, word, type) = line.rstrip().split("\t")
    indexMap[index] = word
fin.close()

fout = open(outfile, "w")

# print html header
fout.write('''\
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<style>
td {
    height: 24px;
    width: 40px;
    padding-left: 5px;
    padding-right: 5px;
    padding-top: 0px;
    padding-bottom: 0px;
}
img {
    width: 20px;
    height: 20px;
}
</style>
</head>
<body>
<table cellspacing="0" cellpadding="0" border="0">
<tr>
  <td id="firstTd" style="padding: 0px"></td>
  <td style="padding: 0px"><div class = "col2" id="divHeader" style="overflow:hidden;width:800px">
  <table cellspacing="0" cellpadding="0" border="1" ><tr>
''')

# print table header, customizable
for col in range(n_keep):
    fout.write('    <td><div class="tableHeader">%d</div></td>\n' % col)
fout.write('    <td><div class="tableHeader">%s</div></td>\n' % "NULL")

# print end of table header, start of freezing column
fout.write('''\
  </tr></table>
  </div></td>
</tr>
<tr>
  <td valign="top" style="padding: 0px"><div id="firstcol" style="overflow: hidden;height:100px">
  <table width="150px" cellspacing="0" cellpadding="0" border="1" >
''')

# print the freezing column(s), customizable
# for row in range(40):
#    fout.write('    <tr><td class="tableFirstCol">%s</td><td class="tableFirstCol">abc</td></tr>\n' % str(row))
# fout.write('    <tr><td class="tableFirstCol">%s</td><td class="tableFirstCol">abc</td></tr>\n' % 'XX')
fin = open(infile, "r")
formatString = '      <td>%s</td>\n'
for emojiId in range(1281):
    if str(emojiId) not in nnMap:
        continue
    fout.write("    <tr class='tableFirstCol'>")
    fout.write(formatString % str(emojiId))
    fout.write(formatString % ("<img src=resource/icons/%s.png>" % str(emojiId)))
    fout.write("      <td>%.2f</td>\n" % (distMap[str(emojiId)]))
    fout.write("    </tr>\n")

fout.write("    <tr>")
fout.write(formatString % "NULL")
fout.write(formatString % "NULL")
fout.write(formatString % "NULL")
fout.write("    </tr>")
fin.close()

# print end of the freezing column(s), start of table content
fout.write('''\
  </table></div>
  </td>
  <td valign="top" style="padding: 0px">
  <div class = "col2" id="table_div" style="overflow: scroll;height:100px;width:800px;position:relative" onscroll="fnScroll()" >
    <table cellspacing="0" cellpadding="0" border="1" >
''')

# print table content, customizable
# for row in range(40):
#    fout.write('    <tr>\n')
#    for col in range(50):
#        fout.write('      <td>%s</td>\n' % str(row * col))
#    fout.write('    </tr>\n')
fin = open(infile, "r")
formatString = '      <td>%s</td>\n'
for emojiId in range(1281):
    if str(emojiId) not in nnMap:
        continue
    nns = nnMap[str(emojiId)]
    fout.write('    <tr>\n')
    for wordId in nns:
        word = indexMap[wordId]
        if len(word) > 20:
            word = word[:18] + "..."
        fout.write(formatString % word)
    fout.write('      <td class="adjH"></td>')
    fout.write('    </tr>\n')
fout.write("<tr>")
for i in range(n_keep):
    fout.write('<td class="adjW"></td></td>')
fout.write("</tr>\n")

# print footer
fout.write('''\
    </table>
  </div>
  </td>
</tr>
</table>
<script>
$(window).resize(function() {adjustSize();});
$(document).ready(function(){fnAdjustTable();adjustSize();});

adjustSize = function(){
  $('.col2').each(function(i){$(this).css("width", $(window).width() - $('#firstcol').width() - 20 - 8*2);});
  $('#firstcol').css("height", $(window).height() - $('#firstTd').height()-20-8*2)
  $('#table_div').css("height", $(window).height() - $('#firstTd').height()-20-8*2)
}

fnAdjustTable = function(){
  var colCount = $('#divHeader>table>tbody>tr>td').length;
  var m = 0; var n = 0; brow = 'mozilla';
  jQuery.each(jQuery.browser, function(i, val) {if(val == true){brow = i.toString();}});
  $('.tableHeader').each(function(i){
    if (m < colCount){
      if (brow == 'mozilla'){$(this).css('width',$('.adjW::eq('+m+')').innerWidth());}
      else if (brow == 'msie'){$('#firstTd').css("width",$('.tableFirstCol').width());$(this).css('width',$('#table_div td:eq('+m+')').width()-2);}
      else if (brow == 'safari'){$(this).css('width',$('.adjW:eq('+m+')').width());}
      else {$('#firstTd').css("width",$('.tableFirstCol').width());$(this).css('width',$('#table_div td:eq('+m+')').innerWidth());}
    }m++;});
  $('.tableFirstCol').each(function(i){
    if(brow == 'mozilla'){$(this).css('height',$('.adjH:eq('+colCount*n+')').outerHeight()+2);}
    else if(brow == 'msie'){$(this).css('height',$('#table_div td:eq('+colCount*n+')').innerHeight()-2);}
    else {$(this).css('height',$('.adjH:eq('+n+')').height()+2);}
    n++;});};

fnScroll = function(){
  $('#divHeader').scrollLeft($('#table_div').scrollLeft());
  $('#firstcol').scrollTop($('#table_div').scrollTop());
};
</script>
</body>
</html>
''')
