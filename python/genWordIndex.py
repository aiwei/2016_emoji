import re
pat = re.compile("^[a-zA-Z@#-]+$")
fout = open("svr/filtered/word_index", "w")
fin = open("svr/index/msg")
for line in fin:
    word, index, cnt = line.rstrip().split("\t")
    if int(cnt) > 2000 and pat.match(word):
        fout.write(index + "\n")
fout.close()