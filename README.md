# Code Sample

This is an code sample of Wei Ai. It is forked from the coding repository of one of my ongoing project, with this README file modified to introduce my code sample. Since this is an ongoing project, the repository may looks "organic".

## Project Overview

In this project, we study how people use emojis from users' keyboard input data. Why do some emojis become especially popular while others don't? How are people using them among the words? In this work, we study the collective usage and behavior of emojis, and specifically, how emojis interact with their context. We base our analysis on a very large corpus collected from a popular emoji keyboard, which contains a complete month of inputs from millions of users. The data are stored on AWS S3 with restricted access. The codes are either run on AWS EMR clusters (as Spark jobs), or on a AWS EC2 server.  

## Code Repository

This git repository stores the codes and scripts for this project, (no data or result is stored here). IntelliJ configuration file is not included in the repository. The following libraries are used:

  - [Stanford CoreNLP](http://stanfordnlp.github.io/CoreNLP/)
  - [Spark](http://spark.apache.org/)

The repository contains four directories:

  - `src` includes the Scala and Java source codes. These codes are developed to process the raw data and extract the useful features for analysis. This directory is compiled into a jar file and uploaded to AWS S3 and run as AWS Elastic Map Reduce (EMR) jobs.
  - `script` includes shell scripts to run codes. Scripts with prefix `aws` are used to the launch EMR clusters and add jobs to the clusters. Scripts with prefix `svr` are used to run Scala on a EC2 server.
  - `python` folder includes some light-weighted tasks that can be easily handled by python, such as visualizing result in a html page. 
  - `jupyter` folder stores IPython Notebooks. Most downstream data analyses are conducted with IPython Notebook (a.k.a. Jupyter Notebook). Since GIT is bad at handling IPython Notebook, I exclude them from GIT repository. For the purpose of code sample, I put a sample notebook under this folder. To see what the notebook looks like. Please use the NBViewer link below:  
    [http://nbviewer.jupyter.org/urls/bitbucket.org/aiwei/2016_emoji/raw/54df0a25cbd71dad40a18a4893430934b0b9238d/jupyter/sementic%20shift%20distribution.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/aiwei/2016_emoji/raw/54df0a25cbd71dad40a18a4893430934b0b9238d/jupyter/sementic%20shift%20distribution.ipynb)
